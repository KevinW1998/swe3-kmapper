﻿using KMapper.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.TestAppUI
{
    public static class MapperInstanceProvider
    {
        private static PSQLMapper mapper = null;

        public static void GlobalInit(PSQLConfiguration config) {
            if (mapper == null)
                mapper = new PSQLMapper(config); 
            else
                throw new InvalidOperationException("Mapper is already init");
        }

        public static PSQLMapper GetInstance() => mapper != null ? mapper : throw new InvalidOperationException("Mapper must be init first");


    }
}

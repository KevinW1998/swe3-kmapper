﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.TestAppUI.Data
{
    public class Book
    {
        [PrimaryKey]
        [AutoIncrement]
        public int? ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime ReleaseDate { get; set; }
    }
}

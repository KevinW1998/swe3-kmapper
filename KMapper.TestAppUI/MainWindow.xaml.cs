﻿using KMapper.Core;
using KMapper.TestAppUI.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KMapper.TestAppUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PSQLMapper mapper;

        public ObservableCollection<Book> LoadAllBooks()
        {
            return new ObservableCollection<Book>(MapperInstanceProvider.GetInstance().Get<Book>());
        }


        public MainWindow()
        {
            InitializeComponent();
            
            // Load the data
            MapperInstanceProvider.GlobalInit(PSQLConfiguration.FromJsonFile("sql-config.json"));

            mapper = MapperInstanceProvider.GetInstance();
            mapper.Open();
            mapper.Register<Book>();
            
            var bookCollection = LoadAllBooks();

            bookCollection.CollectionChanged += BookCollection_CollectionChanged;

            DG1.DataContext = bookCollection;
            
        }

        private void BookCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            
        }
    }
}

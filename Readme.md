# KMapper

KMapper ist ein ORM Mapper geschrieben in C# mit netstandard2.0. Diese Framework unterstützt Postgres als Datenbank.  


Hier sind die wesentlichen Eigenschaften: 
* Sprache: C# (netstandard2.0)
* Datenbank: Postgres
* Mapping-Methode: Code-First (Class --> SQL Script)
* Vererbungsmethode: Table per Type

Funktionen die nicht implementiert wurden:
* Änderungsverfolgung
* Locking

Dokumentiert wurden alle Interfaces (in KMapper.Core/Interfaces) und sonstige vereinzelte Klassen. Unter dem Kapitel "Quick Start" wird erklärt wie die API zu benutzen ist.

## Struktur

### KMapper.Core
KMapper.Core ist das Framework selbst, welches referenziert werden kann. Das Framework und dessen Abhängigkeiten basieren auf netstandard2.0 und ist 
somit sowohl mit einer .net Core Applikation als auch mit dem alten .net Framework Applikation kompatibel. Als in-source referenz kann einfach
der `KMapper.Core`-Ordner kopiert und in einem Projekt eingebunden werden. 

### KMapper.Test
KMapper.Test beinhaltet unit tests für dieses Framework und deckt alle Funktionalitäten ab. Im Ordner ist auch eine Readme.md enthalten, wie diese Umgebung aufgesetzt werden kann, da eine Postgresql-DB benötigt wird. Vor jedem unit test wird die Datenbank gesäubert um zu garantieren, dass keine Nebenwirkungen auftauchen.

Letzte Testergebnisse:


![Testergebnisse](img/testing.PNG)

### KMapper.TestApp
KMapper.TestApp ist eine interaktive Konsolen-Applikation die dazu dient um die performance zu testen. Die gesamte Funktionalität ist **nicht** abgedeckt, sondern dient lediglich zum Performance test. Für einen vollständigen Funktionalitätentest dient *KMapper.Test*. 

#### Beispiel zur Verwendung:

```
Welcome to KMapper Test APP!
Available Commands:
[c] - Connect to Database
[h] - Output help
[q] - Quit application
Enter command [Enter Text]
c
Do you want to use the config sql-config.json? [y/n]
y
Enter command [Enter Text]
h
Available Commands:
[n] - Creates a new entry
[o] - Output all data
[f] - Output all books of a year
[b] - Batch-create 1000 random books with a pseudo-author
[h] - Output help
[q] - Quit application
Enter command [Enter Text]
n
What type? [Select: book, author]
author
Enter author first name [Enter Text]
Max
Enter author last name [Enter Text]
Muster
--------------------------------------------
| ID | FirstName | LastName | WrittenBooks |
--------------------------------------------
| 1  | Max       | Muster   |              |
--------------------------------------------
Enter command [Enter Text]
n
What type? [Select: book, author]
book
Author last name:  [Enter Text]
Muster
Title? [Enter Text]
Das Leben eines Programmierers
Sales? -1 for Unknown [Enter Number]
1000
Enter command [Enter Text]
o
Authors:
--------------------------------------------------------------
| ID | FirstName | LastName | WrittenBooks                   |
--------------------------------------------------------------
| 1  | Max       | Muster   | Das Leben eines Programmierers |
--------------------------------------------------------------
Books:
-------------------------------------------------------------
| ID | Title                          | ReleaseYear | Sales |
-------------------------------------------------------------
| 1  | Das Leben eines Programmierers | 2021        | 1000  |
-------------------------------------------------------------
Action "Get Data" took 11ms
Enter command [Enter Text]
```

Wenn `Enter command [Enter Text]` erscheint, dann kann mittels `h` die Hilfe abgefragt werden. Damit werden alle verfügbaren Kommandos ausgegeben.

## Quick-Start (für Anwender der Library)

### Aufsetzen einer Config
Bevor das Framework verwendet werden kann muss die Konfiguration zur Datenbank definiert werden. Dies kann entweder über eine json-Config umgesetzt werden oder direkt programmier-technisch.

#### Methode A: Json-Config

Im Ordner der Applikation wird eine json erstellt mit folgendem Inhalt (Beispiel `config.json`):
```json
{
  "Host": "localhost",
  "User": "myuser",
  "Password": "pass@myuser",
  "Database": "myexampledb"
}
```

Auf der C#-Seite muss die Config nun geladen werden:
```cs
PSQLConfiguration conf = PSQLConfiguration.FromJsonFile("config.json");
```

#### Methode B: Direct von Code
Die Klasse `PSQLConfiguration` kann natürlich auch direkt erstellt werden:

```cs
PSQLConfiguration conf = new PSQLConfiguration();
conf.Host = "localhost";
conf.User = "myuser";
conf.Password = "pass@myuser";
conf.Database = "myexampledb";
```

### Erstellen der Mapper-Instanz und Registrierung
Jetzt kann eine neue Mapper-Instanz erstellt werden mit der config:

```cs
PSQLMapper mapper = new PSQLMapper(conf);
```

`PSQLMapper` implementiert `IDisposable`. Daher muss nach der Verwendung umbedingt `.Dispose()` ausgeführt werden. Sollte der Mapper nur innerhalb eines Code-Blocks verwendet werden, so kann es mit einem `using`-Block kombiniert werden:

```cs
using (PSQLMapper mapper = new PSQLMapper(config))
{
  // ...
}
```

Sobald die Mapper-Instanz erstellt wurde, muss die Verbindung geöffnet werden und die Klassen, welche gemappt werden sollen, registriert werden:
```cs
mapper.Open();

mapper.Register<SimpleClass>();
```

Bei Klassen mit Vererbung werden die Basis-Klassen **automatisch** registriert. 

### Persistierung eines Objektes

Eine Klasse **muss** einen eindeutigen Identifier haben, d.h. ein Membervariable mit dem Attribut `[PrimaryKey]`. Empfohlen wird:
```cs
[PrimaryKey]
public int Id { get; set; }
```

Der Mapper unterstützt automatisch alle primitiven Datentypen (int, float, string, ...).

Sobald eine Instanz erstellt wurden, kann diese persistiert werden:
```cs
SimpleClass cls = new SimpleClass();
cls.Id = 0;
cls.SimpleValue = 5;
cls.SimpleText = "Hello";

mapper.Persist(cls);
```

`Persist` führt ein Upsert durch, heißt entweder neu eingefügt, oder wenn schon vorhanden, dann überschrieben.

### Caching

Caching kann mittels `mapper.EnableCaching()` aktiviert werden. Wurde schon einmal alle Objekte von der DB geladen, so spring das Caching-System ein.

### Auslesen von Objekten
Sollen alle Objekte von einer Klasse ausgelesen werden, so kann `mapper.Get<...>()` verwendet werden. 
Soll nur einzelen Objekte nach Bedingung ausgelesen werden, so kann `mapper.Get<...>(c => <cond...>)` verwendet werden. 
Beide Methoden unterstützen polymorphes laden, sodass die identität des Objektes gewährleistet wird.

Beispiel:
```cs
IList<SimpleClass> entries = mapper.Get<SimpleClass>(c => c.Id == 0);
```

### Löschen
Löschen von Objekten kann mittels `mapper.Delete(entry)` durchgeführt werden.

### Transaktionen
Transaktionen werden mittels lambda-Funktionen umgesetzt:

```cs
mapper.WithTransaction(t =>
{
    mapper.Persist(instance1);
    mapper.Persist(instance2);
});
```
Die Aktionen in diesem Block wird er dann mit einem Commit bestätigt, sobald der Funktions-Block fertig ausgeführt wurde. 
Eine Exception innerhalb dieses Funktion-Blocks führt zu einem Rollback. Transaktionen können manuell auch bestätigt oder zurückgesetzt werden mit jeweils `t.Commit()` und `t.Rollback()`.

### Contraints

In KMapper werden contraints unterschiedlich abgebildet:
* `NOT NULL`: Alle Felder sind automatisch `NOT NULL`, außer die Membervariable ist explizit mit einem Nullable (?) gekennzeichnet - Beispiel: `public long? HouseId { get; set; }`
* `UNIQUE`: Unique kann mittels `[Unique]` attribut umgesetzt werden.
* `AUTO INCREMENT`: Auto-Increment kann nur mit der Kombination von `[PrimaryKey]` und `[AutoIncrement]` attribut umgesetzt werden. Die Membervariable **muss** nullable declariert sein:
```cs
[PrimaryKey]
[AutoIncrement]
public int? Id { get; set; }
```

### Relationen

Die **1:1 Relation** kann einfach mit einer Membervariable zu der jeweiligen Klassenobjekt realisiert werden:
```cs
public class House
{
    [PrimaryKey]
    public int Id { get; set; }

    public Furniture MainTable { get; set; }
}
```

Die **1:N Relation** wird mittels einer Liste umgesetzt:
```cs
public class ComplexHouse
{
    [PrimaryKey]
    public int Id { get; set; }

    public List<Furniture> Tables { get; set; } = new List<Furniture>();
}
```

Für eine **M:N Relation** müssen beide Seite eine Liste haben, mit einem `[MToN("CommonName")]` Attribut. `CommonName` ist ein platzhalter für den gemeinsamen Namen. Beispiel:

```cs
public class Shop
{
    [PrimaryKey]
    public int Id { get; set; }

    public string Name { get; set; }

    [MToN("ShopCustomers")]
    public List<Customer> Customers { get; set; } = new List<Customer>();

}

public class Customer
{
    [PrimaryKey]
    public int Id { get; set; }

    public string Name { get; set; }

    [MToN("ShopCustomers")]
    public List<Shop> Shops { get; set; } = new List<Shop>();
}
```

**Achtung:** `mapper.Persist` speichert immer nur die direkten Membervariablen. Die Objekte von der jeweiligen Relation muss vorher persistiert werden. 

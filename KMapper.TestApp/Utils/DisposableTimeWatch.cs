﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace KMapper.TestApp.Utils
{
    public class DisposableTimeWatch : IDisposable
    {
        private Stopwatch stopWatch = new Stopwatch();
        private string actionName;

        public DisposableTimeWatch(string actionName)
        {
            stopWatch.Start();
            this.actionName = actionName;
        }

        public void Dispose()
        {
            stopWatch.Stop();

            var elapsed = stopWatch.ElapsedMilliseconds;
            Console.WriteLine($"Action \"{actionName}\" took {elapsed}ms");
        }
    }
}

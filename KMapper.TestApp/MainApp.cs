﻿using KMapper.Core;
using KMapper.TestApp.Data;
using KMapper.TestApp.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using YetAnotherConsoleTables;

namespace KMapper.TestApp
{
    public class MainApp
    {

        private IDictionary<string, (string DescriptionText, Func<bool> Condition, Action Command)> CommandMap = new Dictionary<string, (string, Func<bool>, Action)>();

        private PSQLMapper pSQLMapper = null;

        private bool shouldExit = false;

        public MainApp()
        {

            Func<bool> ConditionIsConnected = () => pSQLMapper != null && pSQLMapper.IsOpen();
            Func<bool> ConditionIsNotConnected = () => !ConditionIsConnected();

            CommandMap.Add("c", ("Connect to Database", ConditionIsNotConnected, () => 
            {
                PSQLConfiguration config = null;
                
                bool useConfig = PromptBool("Do you want to use the config sql-config.json?");
                if (useConfig)
                    config = PSQLConfiguration.FromJsonFile("sql-config.json");
                else
                {
                    config = new PSQLConfiguration();
                    config.Host = PromptString("Enter host");
                    config.Database = PromptString("Enter database");
                    config.User = PromptString("Enter user");
                    config.Password = PromptString("Enter password");
                }

                pSQLMapper = new PSQLMapper(config);
                pSQLMapper.Open();

                pSQLMapper.EnableCaching();

                pSQLMapper.Register<Book>();
                pSQLMapper.Register<Author>();
            }));

            CommandMap.Add("n", ("Creates a new entry", ConditionIsConnected, () => 
            {
                string selection = PromptSelection("What type?", "book", "author");

                if(selection == "book")
                {
                    Book book = new Book();

                    string authorName = PromptString("Author last name: ");
                    var authors = pSQLMapper.Get<Author>(x => x.LastName == authorName);

                    Author selectedAuthor = null;
                    if(authors.Count < 1)
                    {
                        Console.WriteLine("No authors found");
                        return;
                    } 
                    else if(authors.Count > 1)
                    {
                        Console.WriteLine("Multiple authors found, select by ID:");
                        ConsoleTable.From(authors).Write();

                        var id = PromptInt("Select id");

                        selectedAuthor = authors.FirstOrDefault(x => x.ID == id);
                    } 
                    else
                    {
                        selectedAuthor = authors[0];
                    }

                    if(selectedAuthor == null)
                    {
                        Console.WriteLine("No suitable author selected");
                    }

                    selectedAuthor.WrittenBooks.Add(book);

                    book.Title = PromptString("Title?");
                    book.ReleaseYear = DateTime.UtcNow.Year;

                    int salesCount = PromptInt("Sales? -1 for Unknown");
                    if (salesCount != -1)
                        book.Sales = salesCount;

                    pSQLMapper.Persist(book);
                    pSQLMapper.Persist(selectedAuthor); // Update book list
                } 
                else if(selection == "author")
                {
                    Author author = new Author();

                    author.FirstName = PromptString("Enter author first name");
                    author.LastName = PromptString("Enter author last name");

                    pSQLMapper.Persist(author);

                    ConsoleTable.From(new Author[] { author }).Write();
                }
            }));

            CommandMap.Add("o", ("Output all data", ConditionIsConnected, () => {

                using (DisposableTimeWatch _ = new DisposableTimeWatch("Get Data"))
                {
                    var authors = pSQLMapper.Get<Author>();
                    var books = pSQLMapper.Get<Book>();

                    Console.WriteLine("Authors: ");
                    ConsoleTable.From(authors).Write();
                    Console.WriteLine("Books: ");
                    ConsoleTable.From(books).Write();
                }
            }
            ));

            CommandMap.Add("f", ("Output all books of a year", ConditionIsConnected, () => {
                using (DisposableTimeWatch _ = new DisposableTimeWatch("Get Data"))
                {
                    var year = PromptInt("Enter year");
                    var books = pSQLMapper.Get<Book>(b => b.ReleaseYear == year);

                    Console.WriteLine("Books: ");
                    ConsoleTable.From(books).Write();
                }
            }
            ));

            CommandMap.Add("b", ("Batch-create 1000 random books with a pseudo-author", ConditionIsConnected, () => {
                using (DisposableTimeWatch _ = new DisposableTimeWatch("Save data"))
                {
                    Random rnd = new Random();


                    List<Book> savedBooks = new List<Book>();

                    for (int i = 0; i < 1000; i++)
                    {
                        Book b = new Book();
                        b.ReleaseYear = rnd.Next(2010, 2020);
                        b.Sales = rnd.Next(0, 100);
                        b.Title = "The tales of robertus, Chapter " + rnd.Next(0, 10000);

                        pSQLMapper.Persist(b);
                        savedBooks.Add(b);
                    }

                    for(int i = 0; i < 100; i++)
                    {
                        Author robotAuthor = new Author();
                        robotAuthor.FirstName = "Robertus";
                        robotAuthor.LastName = "Maximus-" + Stopwatch.GetTimestamp();

                        pSQLMapper.Persist(robotAuthor);

                        int booksWritten = rnd.Next(1, 10);
                        for(int j = 0; j < booksWritten; j++)
                        {
                            int nextBookIdx = rnd.Next(0, savedBooks.Count);
                            robotAuthor.WrittenBooks.Add(savedBooks[nextBookIdx]);
                            savedBooks.RemoveAt(nextBookIdx);
                        }

                        pSQLMapper.Persist(robotAuthor);
                    }
                    
                }

                Console.WriteLine("Done");
            }));

            // Sys commands
            CommandMap.Add("h", ("Output help", null, () => OutputCommandHelp()));
            CommandMap.Add("q", ("Quit application", null, () =>
            {
                shouldExit = true;
            }
            ));
        }

        private bool PromptBool(string question) {
            while(true)
            {
                Console.WriteLine(question + " [y/n]");
                string result = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(result))
                    continue;
                
                if (result == "y")
                    return true;
                if (result == "n")
                    return false;
            }
        }

        private int PromptInt(string question)
        {
            while (true)
            {
                Console.WriteLine(question + " [Enter Number]");
                string result = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(result))
                    continue;

                if (int.TryParse(result, out int convResult))
                    return convResult;
            }
        }

        private string PromptString(string question)
        {
            while(true)
            {
                Console.WriteLine(question + " [Enter Text]");
                string result = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(result))
                    return result;
            }
        }

        private string PromptSelection(string question, params string[] selections)
        {
            while (true)
            {
                Console.WriteLine($"{question} [Select: {string.Join(", ", selections)}]");
                string result = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(result))
                    continue;

                int idx = Array.IndexOf(selections, result);
                if (idx != -1)
                    return selections[idx];
            }
        }

        private void OutputCommandHelp()
        {
            Console.WriteLine("Available Commands: ");
            foreach (var entry in CommandMap)
            {
                if(entry.Value.Condition == null || entry.Value.Condition())
                {
                    Console.WriteLine($"[{entry.Key}] - {entry.Value.DescriptionText}");
                }
            }
        }

        public void Exec()
        {
            Console.WriteLine("Welcome to KMapper Test APP!");
            OutputCommandHelp();
            while (true)
            {
                string command = PromptString("Enter command");

                if(CommandMap.TryGetValue(command, out var entry)) {
                    if (entry.Condition == null || entry.Condition())
                    {
                        entry.Command();
                    } 
                    else
                    {
                        Console.WriteLine($"Command is not available in this state!");
                    }
                } else
                {
                    Console.WriteLine($"Unknown Command {command}");
                }

                if (shouldExit)
                    break;
            }

            if (pSQLMapper != null)
                pSQLMapper.Dispose();
            pSQLMapper = null;
        }
    }
}

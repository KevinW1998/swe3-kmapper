﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using YetAnotherConsoleTables.Attributes;
using YetAnotherConsoleTables;

namespace KMapper.TestApp.Data
{
    public class Author
    {
        [PrimaryKey]
        [AutoIncrement]
        public int? ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [TableMemberConverter(typeof(BookConverter))]
        public List<Book> WrittenBooks { get; set; } = new List<Book>();

        public override string ToString()
        {
            return $"{FirstName} {LastName} ({ID})";
        }

        private class BookConverter : TableMemberConverter<List<Book>>
        {
            public override string Convert(List<Book> value)
            {
                return string.Join(Environment.NewLine, value.Select(x => x.Title));
            }
        }
    }
}

﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.TestApp.Data
{
    public class Book
    {
        [PrimaryKey]
        [AutoIncrement]
        public int? ID { get; set; }

        public string Title { get; set; }

        public int ReleaseYear { get; set; }

        public int? Sales { get; set; }
    }
}

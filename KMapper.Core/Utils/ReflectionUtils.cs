﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace KMapper.Core.Utils
{
    public static class ReflectionUtils
    {
        public static bool IsGenericList(object o)
        {
            return IsGenericList(o.GetType());
        }

        public static bool IsGenericList(Type oType)
        {
            return (oType.IsGenericType && (oType.GetGenericTypeDefinition() == typeof(List<>)));
        }

        // Source: https://stackoverflow.com/a/33446914
        public static object GetValue(MemberInfo memberInfo, object forObject)
        {
            switch (memberInfo.MemberType)
            {
                case MemberTypes.Field:
                    return ((FieldInfo)memberInfo).GetValue(forObject);
                case MemberTypes.Property:
                    return ((PropertyInfo)memberInfo).GetValue(forObject);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KMapper.Core
{
    /// <summary>
    /// Contains (deserializable) configuration information that is needed to connect to
    /// a postgres database.
    /// </summary>
    public class PSQLConfiguration
    {
        /// <summary>
        /// The hostname/ip of the target server.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// The username to use
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// The password to use
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The database to connect to
        /// </summary>
        public string Database { get; set; }

        /// <summary>
        /// If error messages shall contain detailed messages
        /// </summary>
        public bool EnableDetailed { get; set; } = false;

        /// <summary>
        /// Creates a PSQLConfiguration from a json file.
        /// </summary>
        /// <param name="path">The path to the json file</param>
        /// <returns>PSQLConfiguration instance</returns>
        public static PSQLConfiguration FromJsonFile(string path)
        {
            return JsonConvert.DeserializeObject<PSQLConfiguration>(File.ReadAllText(path));
        }
    }
}

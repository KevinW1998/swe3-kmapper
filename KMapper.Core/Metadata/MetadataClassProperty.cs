﻿using KMapper.Core.Attributes;
using KMapper.Core.Exceptions;
using KMapper.Core.Utils;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace KMapper.Core.Metadata
{
    /// <summary>
    /// Contains metadata of a property field that is needed for DB-Side
    /// </summary>
    public class MetadataClassProperty
    {
        /// <summary>
        /// Is primary key
        /// </summary>
        public bool IsPrimaryKey { get; }

        /// <summary>
        /// Is auto increment
        /// </summary>
        public bool IsAutoIncrement { get; }

        /// <summary>
        /// Is unique
        /// </summary>
        public bool IsUnique { get; }

        /// <summary>
        /// The column name of the field
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The underlying type
        /// </summary>
        public Type Type { get; }

        /// <summary>
        /// The property field in the class that this field is contained in
        /// </summary>
        public PropertyInfo PropertyInfo { get; }

        /// <summary>
        /// Is not null
        /// </summary>
        public bool IsNotNull { get; }

        /// <summary>
        /// The parent class that owns this field
        /// </summary>
        public MetadataClass Parent { get; }

        /// <summary>
        /// Creates a new metadata class property instance containing useful information for the DB.
        /// </summary>
        /// <param name="propertyInfo">The property info</param>
        /// <param name="parent">The parent class that owns this field</param>
        public MetadataClassProperty(PropertyInfo propertyInfo, MetadataClass parent)
        {
            IsPrimaryKey = propertyInfo.GetCustomAttribute(typeof(PrimaryKey)) != null;
            IsAutoIncrement = propertyInfo.GetCustomAttribute(typeof(AutoIncrement)) != null;
            IsUnique = propertyInfo.GetCustomAttribute(typeof(Unique)) != null;
            Name = propertyInfo.Name.ToLower();
            Type = propertyInfo.PropertyType;
            PropertyInfo = propertyInfo;
            IsNotNull = !StackOverflowUtils.IsNullable(propertyInfo);
            Parent = parent;

            if (IsPrimaryKey && (!IsNotNull && !IsAutoIncrement))
                throw new MapperException($"Primary key {Name} is declared as nullable!", Type);

            if (!IsPrimaryKey && IsAutoIncrement)
                throw new MapperException($"Only primary keys can be auto increment!", Type);

            if (IsPrimaryKey && IsNotNull && IsAutoIncrement)
                throw new MapperException($"Auto increment primary key must be declared as nullable!", Type);

            if(IsAutoIncrement)
            {
                var unpackedType = Nullable.GetUnderlyingType(Type);
                if (unpackedType != typeof(int))
                    throw new MapperException($"Auto increment primary key must be int", Type);
            }
        }
    }
}

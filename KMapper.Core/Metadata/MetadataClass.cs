﻿using KMapper.Core.Attributes;
using KMapper.Core.Exceptions;
using KMapper.Core.Interfaces;
using KMapper.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Text;

namespace KMapper.Core.Metadata
{
    /// <summary>
    /// Contains metadata of a type that is needed for DB-Side
    /// </summary>
    public class MetadataClass
    {
        private static ICollection<Type> PrimitivePropertyTypes = new List<Type>()
        {
            typeof(string),
            typeof(DateTime)
        }.AsReadOnly();

        /// <summary>
        /// Contains all data fields (without primary key)
        /// </summary>
        private List<MetadataClassProperty> privimiteProperties = new List<MetadataClassProperty>();

        /// <summary>
        /// Contains 1:1 relation (non-owning)
        /// </summary>
        private List<MetadataClassRelationProperty> simpleRelationProperties = new List<MetadataClassRelationProperty>();

        /// <summary>
        /// Contains 1:N relations (non-owning)
        /// </summary>
        private List<MetadataClassListRelationProperty> nRelationProperties = new List<MetadataClassListRelationProperty>();

        /// <summary>
        /// Contains M:N relations (non-owning)
        /// </summary>
        private List<MetadataClassMToNRelationProperty> mToNRelationProperties = new List<MetadataClassMToNRelationProperty>();

        /// <summary>
        /// Whether the type is already available on the DB-Side
        /// </summary>
        public bool IsTableCreated { get; set; } = false;

        /// <summary>
        /// The name of the table on the DB-Side
        /// </summary>
        public string ClassName { get; }

        /// <summary>
        /// Contains all data fields (without primary key)
        /// </summary>
        public IList<MetadataClassProperty> PrimitiveProperties => privimiteProperties.AsReadOnly();

        /// <summary>
        /// Contains 1:1 relation (non-owning)
        /// </summary>
        public IList<MetadataClassRelationProperty> SimpleRelationProperties => simpleRelationProperties.AsReadOnly();

        /// <summary>
        /// Contains 1:N relations (non-owning)
        /// </summary>
        public IList<MetadataClassListRelationProperty> NRelationProperties => nRelationProperties.AsReadOnly();

        /// <summary>
        /// Contains M:N relations (non-owning)
        /// </summary>
        public IList<MetadataClassMToNRelationProperty> MToNRelationProperties => mToNRelationProperties.AsReadOnly();

        /// <summary>
        /// The underlying type that the metadata class is wrapping.
        /// </summary>
        public Type EnclosingType { get; }

        /// <summary>
        /// Meta infos of the primary key
        /// </summary>
        public MetadataClassProperty PrimaryKey { get; }

        /// <summary>
        /// True if the primary key is auto-increment
        /// </summary>
        public bool IsPrimaryKeyAutoIncrement  => PrimaryKey.IsAutoIncrement;

        /// <summary>
        /// If available, the BaseType. Can be null
        /// </summary>
        public MetadataClass BaseType { get; }

        /// <summary>
        /// Whether the type inherits from a base class
        /// </summary>
        public bool HasBaseType => BaseType != null;

        /// <summary>
        /// Classes that derived from this type
        /// </summary>
        public IList<MetadataClass> DerivedTypes { get; } = new List<MetadataClass>();

        /// <summary>
        /// If there are classes that derive from this type
        /// </summary>
        public bool HasDerivedTypes => DerivedTypes.Count > 0;

        public MetadataClass(Type enclosingType, IMetadataClassProvider metadataClassProvider)
        {
            EnclosingType = enclosingType;

            // Reflect type
            PropertyInfo[] propInfos = enclosingType.GetProperties();

            foreach (PropertyInfo propInfo in propInfos)
            {
                if(propInfo.DeclaringType == enclosingType) // Only gather property info of the actual class
                {
                    Type propertyType = propInfo.PropertyType;
                    propertyType = Nullable.GetUnderlyingType(propertyType) ?? propertyType; // Get underlying type, if nullable

                    if (!propInfo.CanWrite)
                        throw new MapperException($"Property {propInfo.Name} must be writeable", propInfo.PropertyType);

                    if (!propInfo.CanRead)
                        throw new MapperException($"Property {propInfo.Name} must be readable", propInfo.PropertyType);

                    if (propertyType.IsPrimitive || PrimitivePropertyTypes.Contains(propertyType))
                    {
                        privimiteProperties.Add(new MetadataClassProperty(propInfo, this));
                    }
                    else if (propertyType.IsValueType)
                    {
                        throw new MapperException("Custom value types or structs are not supported", propertyType);
                    }
                    else
                    {
                        // Ref as 1:N
                        bool isList = ReflectionUtils.IsGenericList(propertyType);
                        if(isList)
                        {
                            Type targetType = propertyType.GetGenericArguments()[0];

                            // 1:N or M:N
                            MToN multiListInfo = propInfo.GetCustomAttribute<MToN>();
                            if(multiListInfo != null)
                            {
                                // Try to get the other side
                                if (FindMToNRelationPropertyByName(multiListInfo.CommonName) != null)
                                    throw new MapperException($"M to N relation with common name {multiListInfo.CommonName} already exists in this type", targetType);

                                var newProp = new MetadataClassMToNRelationProperty(propInfo, multiListInfo.CommonName, this);

                                // Look if the other side is already registered, if yes, resolve both
                                if(metadataClassProvider.TryGet(targetType, out MetadataClass metadata))
                                {
                                    var propOtherSide = metadata.FindMToNRelationPropertyByName(multiListInfo.CommonName);
                                    if (propOtherSide == null)
                                        throw new MapperException($"Missing M to N relation list for the other type {targetType} from type {metadata.ClassName}", targetType);

                                    // Resolve both sides
                                    propOtherSide.Resolve(this);
                                    newProp.Resolve(metadata);
                                }

                                mToNRelationProperties.Add(newProp);
                            }
                            else
                            {
                                if (!metadataClassProvider.TryGet(targetType, out MetadataClass metadata))
                                    throw new MapperException($"Type {targetType} is not registered and cannot be used for a 1:N relation", targetType);

                                nRelationProperties.Add(new MetadataClassListRelationProperty(propInfo, metadata, this));
                            }
                        }
                        else {
                            if (!metadataClassProvider.TryGet(propertyType, out MetadataClass metadata))
                                throw new MapperException($"Type {propertyType.Name} is not registered and cannot be used for a simple relation", propertyType);

                            simpleRelationProperties.Add(new MetadataClassRelationProperty(propInfo, metadata, this));
                        }

                    }
                }
            }

            ClassName = enclosingType.Name.ToLower();

            // Check if inherits
            Type baseType = enclosingType.BaseType;
            if(baseType != typeof(object))
            {
                BaseType = metadataClassProvider.GetOrRegister(baseType);
                PrimaryKey = BaseType.PrimaryKey; // Inherit Primary Key

                BaseType.DerivedTypes.Add(this);
            }

            // Validate
            if(!HasBaseType)
            {
                bool hasPrimaryKey = false;
                foreach (var metadataField in privimiteProperties)
                {
                    if (metadataField.IsPrimaryKey)
                    {
                        if (hasPrimaryKey)
                            throw new MapperException($"Type {enclosingType.Name} has multiple primary keys", enclosingType);
                        hasPrimaryKey = true;
                        PrimaryKey = metadataField;
                    }
                }

                if (!hasPrimaryKey)
                    throw new MapperException($"Type {enclosingType.Name} must have at least one primary key", enclosingType);

                // Remove Primary Key From Main List
                privimiteProperties.Remove(PrimaryKey);
            }

            // TODO: Validate property with same name, but different case
            // i.e. HelloWorld
            // vs
            //      helloworld

            // Validate default constructor [Note: Might be even caugth at compile time, but better safe than sorry]
            if (enclosingType.GetConstructor(Type.EmptyTypes) == null)
                throw new MapperException($"Type {enclosingType.Name} must provide a paremeterless constructor", enclosingType);
        }

        public MetadataClassProperty TryFindPropertyRecurse(string name)
        {
            if (PrimaryKey.Name == name)
                return PrimaryKey;

            foreach(var prop in privimiteProperties)
                if (prop.Name == name)
                    return prop;

            if (HasBaseType)
                return BaseType.TryFindPropertyRecurse(name);

            return null;
        }

        private IEnumerable<MetadataClassProperty> QueryPropertiesInternal(MetadataClassQueryFlags queryFlags, bool root)
        {
            IEnumerable<MetadataClassProperty> props = queryFlags.HasFlag(MetadataClassQueryFlags.Recurse) && HasBaseType ? BaseType.QueryPropertiesInternal(queryFlags, false) : Enumerable.Empty<MetadataClassProperty>();

            if (queryFlags.HasFlag(MetadataClassQueryFlags.Primitive))
                props = props.Concat(privimiteProperties);

            if (queryFlags.HasFlag(MetadataClassQueryFlags.SimpleRelations))
                props = props.Concat(simpleRelationProperties);

            if (queryFlags.HasFlag(MetadataClassQueryFlags.OneToNRelations))
                props = props.Concat(nRelationProperties);

            if (queryFlags.HasFlag(MetadataClassQueryFlags.MToNRelations))
                props = props.Concat(mToNRelationProperties);

            if (root && queryFlags.HasFlag(MetadataClassQueryFlags.PrimaryKey))
                props = props.Append(PrimaryKey);

            return props;
        }

        /// <summary>
        /// Returns a list of fields depending on the query flags
        /// </summary>
        /// <param name="queryFlags">The given query flags</param>
        /// <returns>An iteratable containing the fields matching the flags</returns>
        /// <seealso cref="MetadataClassQueryFlags"/>
        public IEnumerable<MetadataClassProperty> QueryProperties(MetadataClassQueryFlags queryFlags = MetadataClassQueryFlags.Primitive)
        {
            return QueryPropertiesInternal(queryFlags, true);
        }

        /// <summary>
        /// Checks if the given type has no further fields (apart from the pimrary key)
        /// </summary>
        /// <returns>True, if no further fields</returns>
        public bool HasNoFields()
        {
            return QueryProperties(MetadataClassQueryFlags.AllPropertiesWithoutPrimary).FirstOrDefault() == null;
        }

        /// <summary>
        /// Returns the base type that doesn't derive from any other class. 
        /// "this", will be returned, if the current target type doesn't dervie from anything.
        /// </summary>
        /// <returns></returns>
        public MetadataClass GetRootBaseType()
        {
            return HasBaseType ? BaseType.GetRootBaseType() : this;
        }

        /// <summary>
        /// Returns true, if the target type has any relations (1:1, 1:N, M:N)
        /// </summary>
        /// <returns>True, if containing relations</returns>
        public bool HasRelations() {
            return QueryProperties(MetadataClassQueryFlags.AllRelations).FirstOrDefault() != null;
        }

        /// <summary>
        /// Returns the M:N relation property matching by the common name
        /// </summary>
        /// <param name="name">The common name of the M:N relation</param>
        /// <returns>The M:N relation property</returns>
        public MetadataClassMToNRelationProperty FindMToNRelationPropertyByName(string name)
        {
            return mToNRelationProperties.Find(x => x.CommonTableName == name);
        }
    }
}

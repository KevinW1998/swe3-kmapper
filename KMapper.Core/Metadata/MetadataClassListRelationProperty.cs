﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace KMapper.Core.Metadata
{
    /// <summary>
    /// Represents a 1:N relation mapped as a List<T>
    /// </summary>
    public class MetadataClassListRelationProperty : MetadataClassRelationProperty
    {
        /// <summary>
        /// The type T of List<T>
        /// </summary>
        public Type ContainerType { get; }

        /// <summary>
        /// The foreign key name on the other side
        /// </summary>
        public string ForeignKeyName => "list_entry_" + Name;

        /// <summary>
        /// Creates a new list relation property
        /// </summary>
        /// <param name="propertyInfo">The property info</param>
        /// <param name="relatesTo">The class to which it has a relation to. Can be null if not resolved yet.</param>
        /// <param name="parent">The parent class that owns this field</param>
        public MetadataClassListRelationProperty(PropertyInfo propertyInfo, MetadataClass relatesTo, MetadataClass parent) : base(propertyInfo, relatesTo, parent)
        {
            ContainerType = propertyInfo.PropertyType.GetGenericArguments()[0];
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace KMapper.Core.Metadata
{
    public class MetadataClassMToNRelationProperty : MetadataClassListRelationProperty
    {
        /// <summary>
        /// The common shared table name for both M- and N-Side
        /// </summary>
        public string CommonTableName { get; }

        /// <summary>
        /// If the N-Side is already resolved
        /// </summary>
        /// <seealso cref="MetadataClassRelationProperty.RelatesTo"/>
        public bool IsResolved => RelatesTo != null;

        /// <summary>
        /// Resolves the N-Side
        /// </summary>
        /// <param name="metadataClassTargetType">The given N-Side</param>
        public void Resolve(MetadataClass metadataClassTargetType)
        {
            if (IsResolved)
                throw new InvalidOperationException($"Property {Name} of {Parent.ClassName} is already resolved!");

            RelatesTo = metadataClassTargetType;
        }

        /// <summary>
        /// Creates a new M:N relation property. The N-Side is not initially resolved yet.
        /// </summary>
        /// <param name="propertyInfo">The property info</param>
        /// <param name="commonName">The common shared table name for both M- and N-Side</param>
        /// <param name="parent">The parent class that owns this field</param>
        public MetadataClassMToNRelationProperty(PropertyInfo propertyInfo, string commonName, MetadataClass parent) : base(propertyInfo, null, parent)
        {
            CommonTableName = commonName;
        }
    }
}

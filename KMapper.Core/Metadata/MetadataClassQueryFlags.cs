﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Metadata
{
    /// <summary>
    /// Query flags for collecting fields and relations
    /// </summary>
    /// <seealso cref="MetadataClass.QueryProperties(MetadataClassQueryFlags)"/>
    [Flags]
    public enum MetadataClassQueryFlags
    {
        /// <summary>
        /// Collect also from base clases
        /// </summary>
        Recurse = 1,

        /// <summary>
        /// Collect primitive fields
        /// </summary>
        Primitive = 2,

        /// <summary>
        /// Collection simple relations
        /// </summary>
        SimpleRelations = 4,
        
        /// <summary>
        /// Collect 1:N relations
        /// </summary>
        OneToNRelations = 8,

        /// <summary>
        /// Collect M:N relations
        /// </summary>
        MToNRelations = 16,

        /// <summary>
        /// Collect primary key
        /// </summary>
        PrimaryKey = 32,

        // Specials

        /// <summary>
        /// All relations (1:1, 1:N, M:N)
        /// </summary>
        AllRelations = SimpleRelations | OneToNRelations | MToNRelations,

        /// <summary>
        /// All properties (Primitive + Relations) without primary key
        /// </summary>
        AllPropertiesWithoutPrimary = Primitive | AllRelations,

        /// <summary>
        /// All properties (Primitive + Relations) with primary key
        /// </summary>
        AllProperties = AllPropertiesWithoutPrimary | PrimaryKey,

    }
}

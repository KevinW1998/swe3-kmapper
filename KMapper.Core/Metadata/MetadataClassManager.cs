﻿using KMapper.Core.Exceptions;
using KMapper.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Metadata
{
    /// <summary>
    /// Implementation for metadata class provider
    /// </summary>
    public class MetadataClassManager : IMetadataClassProvider
    {

        private IDictionary<Type, MetadataClass> registedMetadataClasses = new Dictionary<Type, MetadataClass>();

        public void Register(Type type)
        {
            registedMetadataClasses.Add(type, new MetadataClass(type, this));
        }

        public MetadataClass GetOrRegister(Type type)
        {
            if(!registedMetadataClasses.TryGetValue(type, out MetadataClass retVal))
            {
                retVal = new MetadataClass(type, this);
                registedMetadataClasses.Add(type, retVal);
            }
            return retVal;
        }

        public MetadataClass Get(Type type)
        {
            if (!registedMetadataClasses.TryGetValue(type, out MetadataClass retVal))
                throw new MapperException($"Type {type.Name} is not registered", type);
            
            return retVal;
        }

        public bool TryGet(Type type, out MetadataClass metadata)
        {
            return registedMetadataClasses.TryGetValue(type, out metadata);
        }
    }
}

﻿using KMapper.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace KMapper.Core.Metadata
{
    /// <summary>
    /// Represents a simple relation
    /// </summary>
    public class MetadataClassRelationProperty : MetadataClassProperty
    {
        /// <summary>
        /// The class to which it has a relation to
        /// </summary>
        public MetadataClass RelatesTo { get; protected set; }

        /// <summary>
        /// Creates a new relation property
        /// </summary>
        /// <param name="propertyInfo">The property info</param>
        /// <param name="relatesTo">The class to which it has a relation to. Can be null if not resolved yet.</param>
        /// <param name="parent">The parent class that owns this field</param>
        public MetadataClassRelationProperty(PropertyInfo propertyInfo, MetadataClass relatesTo, MetadataClass parent) : base(propertyInfo, parent)
        {
            if (IsPrimaryKey)
                throw new MapperException("Relation cannot be a primary key itself.", Type);

            if (IsAutoIncrement)
                throw new MapperException("Relation cannot be auto increment.", Type);

            RelatesTo = relatesTo;
        }
    }
}

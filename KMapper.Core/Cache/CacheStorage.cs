﻿using KMapper.Core.Interfaces;
using KMapper.Core.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace KMapper.Core.Cache
{
    public class SimpleCacheStorage : IDataStorage
    {
        private IDataStorage targetStorage;
        private IMetadataClassProvider metadataClassProvider;

        // TODO: Map by PK
        private Dictionary<Type, CacheInfo> cacheElements = new Dictionary<Type, CacheInfo>();

        // Transaction aware code
        private List<object> objectsInTransaction = new List<object>();
        private bool isTransactionActive = false;


        public SimpleCacheStorage(IDataStorage targetStorage, IMetadataClassProvider metadataClassProvider)
        {
            this.targetStorage = targetStorage;
            this.metadataClassProvider = metadataClassProvider;
        }

        private PropertyInfo GetPrimaryKeyPropertyInfo(Type type)
        {
            return metadataClassProvider.Get(type).PrimaryKey.PropertyInfo;
        }

        private CacheInfo GetCacheInfoOrCreate(Type targetType)
        {
            if (!cacheElements.TryGetValue(targetType, out CacheInfo cacheInfo))
            {
                cacheInfo = new CacheInfo(targetType, GetPrimaryKeyPropertyInfo(targetType));
                cacheElements.Add(targetType, cacheInfo);
            }
            return cacheInfo;
        }

        private void SetAllRecurse<T>(IList<T> entries, MetadataClass metadata)
        {
            CacheInfo cacheInfo = GetCacheInfoOrCreate(metadata.EnclosingType);

            if (typeof(T) == metadata.EnclosingType)
            {
                cacheInfo.Set(entries);
            }
            else
            {
                // Filter out derived objects
                var assignableEntries = entries.Where(v => metadata.EnclosingType.IsAssignableFrom(v.GetType()));

                cacheInfo.SetObjects(assignableEntries.Cast<object>());
            }
            cacheInfo.HasReadCache = true;

            foreach (MetadataClass derivedMetadataClass in metadata.DerivedTypes)
                SetAllRecurse(entries, derivedMetadataClass);
        }

        public IList<T> GetAll<T>()
        {
            Type targetType = typeof(T);

            if (cacheElements.ContainsKey(targetType)) {
                if (cacheElements[targetType].HasReadCache)
                    return cacheElements[targetType].Get<T>(); // Use Cache
            }

            var retVal = targetStorage.GetAll<T>(); // DB Operation

            // Store Result in Cache
            SetAllRecurse(retVal, metadataClassProvider.Get(typeof(T)));

            return retVal;
        }

        private void UpsertRecurse(object data, MetadataClass metadata)
        {
            CacheInfo cacheInfo = GetCacheInfoOrCreate(metadata.EnclosingType);
            cacheInfo.Add(data);

            if (metadata.HasBaseType)
                UpsertRecurse(data, metadata.BaseType);
        }

        public void Upsert(object data)
        {
            targetStorage.Upsert(data);  // DB Operation

            if (isTransactionActive)
                objectsInTransaction.Add(data);
            else
                UpsertRecurse(data, metadataClassProvider.Get(data.GetType()));
        }

        public void Register(MetadataClass metadataClass) => targetStorage.Register(metadataClass);

        private void TryDeleteRecurse(object data, MetadataClass metadata)
        {
            CacheInfo cacheInfo = GetCacheInfoOrCreate(metadata.EnclosingType);
            cacheInfo.TryRemove(data);

            if (metadata.HasBaseType)
                TryDeleteRecurse(data, metadata.BaseType);
        }

        public void Delete(object data) {

            targetStorage.Delete(data);

            TryDeleteRecurse(data, metadataClassProvider.Get(data.GetType()));
        }

        public IList<T> GetWhere<T>(ISQLQueryExtraData parsedWhere, Expression<Func<T, bool>> origWhereExpression)
        {
            Type targetType = typeof(T);

            if (cacheElements.ContainsKey(targetType))
            {
                if (cacheElements[targetType].HasReadCache)
                    return cacheElements[targetType].Get<T>().Where(origWhereExpression.Compile()).ToList(); // Use Cache
            }

            var retVal = targetStorage.GetWhere(parsedWhere, origWhereExpression); // DB Operation

            // Store Result in Cache
            // SetAllRecurse(retVal, metadataClassProvider.Get(typeof(T)));

            return retVal;
        }

        public void BeginTransaction() {
            isTransactionActive = true;
            targetStorage.BeginTransaction();
        }

        public void Commit()
        {
            isTransactionActive = false;

            // We use this method to clear out the objects in objectsInTransaction
            // no matter if Commit is successful or fails.
            // If successful, then the objects can be pushed into the cache
            List<object> objectsInTransactionLocal = objectsInTransaction;
            objectsInTransaction = new List<object>();

            targetStorage.Commit();
            foreach(object objectsToUpsert in objectsInTransactionLocal)
                UpsertRecurse(objectsToUpsert, metadataClassProvider.Get(objectsToUpsert.GetType()));
        }

        public void Rollback()
        {
            isTransactionActive = false;
            targetStorage.Rollback();
        }
    }
}

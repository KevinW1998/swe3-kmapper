﻿using KMapper.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace KMapper.Core.Cache
{
    /// <summary>
    /// Contains a cache of objects for a specific type
    /// </summary>
    internal class CacheInfo
    {

        private IDictionary<object, object> CacheEntries = new Dictionary<object, object>();

        private Type type;

        private PropertyInfo propertyInfoPrimaryKey;

        public bool HasReadCache { get; set; } = false;

        private void ValidateType(Type targetType)
        {
            if (!type.IsAssignableFrom(targetType))
                throw new MapperException($"Type validation failed. Expected {type.Name}, got {targetType.Name}", targetType);
        }

        public CacheInfo(Type type, PropertyInfo propertyInfoPrimaryKey)
        {
            this.type = type;
            this.propertyInfoPrimaryKey = propertyInfoPrimaryKey;
        }

        public void Add(object data)
        {
            ValidateType(data.GetType());

            CacheEntries[propertyInfoPrimaryKey.GetValue(data)] = data;
        }

        public void Set<T>(IEnumerable<T> data)
        {
            ValidateType(typeof(T));

            CacheEntries.Clear();
            foreach (T value in data)
                CacheEntries.Add(propertyInfoPrimaryKey.GetValue(value), value);
        }

        public void SetObjects(IEnumerable<object> data)
        {

            CacheEntries.Clear();
            foreach(object value in data)
            {
                ValidateType(value.GetType());
                CacheEntries.Add(propertyInfoPrimaryKey.GetValue(value), value);
            }
        }

        public IList<T> Get<T>()
        {
            ValidateType(typeof(T));

            return new List<T>(CacheEntries.Values.Cast<T>());
        }

        public bool TryRemove(object data)
        {
            ValidateType(data.GetType());

            return CacheEntries.Remove(propertyInfoPrimaryKey.GetValue(data));
        }
    }
}

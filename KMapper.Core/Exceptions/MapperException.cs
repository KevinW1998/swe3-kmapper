﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Exceptions
{
    public class MapperException : Exception
    {

        public Type FailingType { get; }

        public MapperException(string message, Type failingType) : base(message)
        {
            FailingType = failingType;
        }
    }
}

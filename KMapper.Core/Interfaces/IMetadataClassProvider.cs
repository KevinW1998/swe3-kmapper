﻿using KMapper.Core.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Interfaces
{
    public interface IMetadataClassProvider
    {
        /// <summary>
        /// Registers a type and creates metadata for it.
        /// </summary>
        /// <param name="type"></param>
        void Register(Type type);

        /// <summary>
        /// Tries to get the metadata of a Type or registers it.
        /// </summary>
        /// <param name="type">The given type to get the metadata for.</param>
        /// <returns></returns>
        MetadataClass GetOrRegister(Type type);

        /// <summary>
        /// Get the metadata of a type.
        /// </summary>
        /// <param name="type">The given type to get the metadata for.</param>
        /// <exception cref="MapperException">Type is not registered</exception>
        /// <returns></returns>
        MetadataClass Get(Type type);

        /// <summary>
        /// Tries to get the metadata of a type.
        /// </summary>
        /// <param name="type">The given type to get the metadata for.</param>
        /// <param name="metadata">Metadata output</param>
        /// <returns>True if a registered type was found</returns>
        bool TryGet(Type type, out MetadataClass metadata);
    }
}

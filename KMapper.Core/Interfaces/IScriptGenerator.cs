﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace KMapper.Core.Interfaces
{
    /// <summary>
    /// A script generator provider that converts expressions to sql queries
    /// </summary>
    public interface IScriptGenerator
    {
        /// <summary>
        /// Creates an sql query from the given expression
        /// </summary>
        /// <typeparam name="T">Target type</typeparam>
        /// <param name="whereClause">The where part</param>
        /// <exception cref="NotSupportedException">If the given expression part is not supported</exception>
        /// <returns>The converted query</returns>
        ISQLQueryExtraData CreateQueryFromExpression<T>(Expression<Func<T, bool>> whereClause);
    }
}

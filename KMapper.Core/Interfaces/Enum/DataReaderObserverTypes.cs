﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Interfaces.Enum
{
    /// <summary>
    /// Marks what the data reader observer shall listen for
    /// <see cref="IDataReaderObserver"/>
    /// </summary>
    [Flags]
    internal enum DataReaderObserverTypes
    {
        /// <summary>
        /// Mapped fields that can be resolved
        /// </summary>
        MappedFields = 1,
        /// <summary>
        /// Fields that cannot be mapped and unknown for the target class (i.e. aggregate values)
        /// </summary>
        UnknownFields = 2
    }
}

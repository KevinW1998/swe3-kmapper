﻿using KMapper.Core.Metadata;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace KMapper.Core.Interfaces
{
    public interface IDataStorage : ITransactionProvider
    {
        /// <summary>
        /// Registers the meta data class to the storage.
        /// </summary>
        /// <param name="metadataClass">The metedata class to register</param>
        void Register(MetadataClass metadataClass);

        /// <summary>
        /// Inserts or updates an object to the storage
        /// </summary>
        /// <param name="data">The object to store</param>
        void Upsert(object data);

        /// <summary>
        /// Removes an object from the storage
        /// </summary>
        /// <param name="data">The object to remove</param>
        void Delete(object data);

        /// <summary>
        /// Get all objects (including dervied ones) from type T from the storage
        /// </summary>
        /// <typeparam name="T">The type identifier</typeparam>
        /// <returns>All objects of that type from the storage</returns>
        IList<T> GetAll<T>();

        /// <summary>
        /// Gets all objects that matches the filter expression
        /// </summary>
        /// <typeparam name="T">The type identifier</typeparam>
        /// <param name="parsedWhere">The WHERE-part of the sql statements with the parameters included</param>
        /// <param name="origWhereExpression">The original expression of the where part</param>
        /// <returns></returns>
        IList<T> GetWhere<T>(ISQLQueryExtraData parsedWhere, Expression<Func<T, bool>> origWhereExpression);
    }
}

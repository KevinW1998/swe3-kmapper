﻿using KMapper.Core.Interfaces.Data;
using KMapper.Core.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Interfaces
{
    /// <summary>
    /// Contains additional information that is required for the given query.
    /// </summary>
    public interface ISQLQueryExtraData
    {
        /// <summary>
        /// The WHERE-Part of the query
        /// </summary>
        string WhereQuery { get; }

        /// <summary>
        /// Parameters to be mapped and decoded. (Needs to be part of the WHERE-part)
        /// </summary>
        IReadOnlyDictionary<string, object> Parameters { get; }

        /// <summary>
        /// References to other tables that needs to be joined (NATURAL INNER JOIN)
        /// </summary>
        IReadOnlyList<MetadataClass> RequiredNaturalInnerJoinWith { get; }

        /// <summary>
        /// Contains extra definitons for INNER JOIN ON
        /// </summary>
        IReadOnlyList<ExtraDataInnerJoinDefinition> RequiredInnerJoinsWith { get; }

        /// <summary>
        /// Extra fields for the SELECT-part
        /// </summary>
        IReadOnlyList<string> OptionalExtraSelectFields { get; }
    }
}

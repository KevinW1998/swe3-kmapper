﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Interfaces
{
    public interface IPrimitiveMappingProvider
    {
        /// <summary>
        /// Get the equivalent DB-Type to from the primitive C#-Type.
        /// </summary>
        /// <param name="type">The given type</param>
        /// <returns>The equivalent DB-Type</returns>
        string GetDbTypeAsString(Type type);
    }
}

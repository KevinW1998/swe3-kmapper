﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Interfaces.Data
{
    /// <summary>
    /// Contains information about additional inner join commands.
    /// </summary>
    public class ExtraDataInnerJoinDefinition
    {
        /// <summary>
        /// The field of the own table to join on
        /// </summary>
        public string OwnTableField { get; set; } = "";

        /// <summary>
        /// The field of the foreign table to join on
        /// </summary>
        public string ForeignTableField { get; set; } = "";

        /// <summary>
        /// The foreign table name
        /// </summary>
        public string ForeignTable { get; set; } = "";
    }
}

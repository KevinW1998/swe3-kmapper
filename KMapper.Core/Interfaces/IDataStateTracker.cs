﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Interfaces
{
    public interface IDataStateTracker
    {
        /// <summary>
        /// Checks if the object is tracked and stored in the DB-Side
        /// </summary>
        /// <param name="data">The data to check</param>
        /// <returns>If it is tracked</returns>
        bool IsTracked(object data);

        /// <summary>
        /// Marks the object that it is stored in DB-Side
        /// </summary>
        /// <param name="data">The object to track</param>
        void Track(object data);

        /// <summary>
        /// Unmarks the object that it is stored in DB-Side
        /// </summary>
        /// <param name="data">The object to untrack</param>
        void Untrack(object data);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Interfaces
{
    /// <summary>
    /// Provides an transaction interfaces
    /// </summary>
    public interface ITransactionProvider
    {
        /// <summary>
        /// Starts the transaction 
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// Commits the changes
        /// </summary>
        void Commit();

        /// <summary>
        /// Rollbacks the changes inside of the transaction
        /// </summary>
        void Rollback();
    }
}

﻿using KMapper.Core.Interfaces.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Interfaces
{
    /// <summary>
    /// Observer interface to listen for data that is being read from the sql data stream
    /// </summary>
    internal interface IDataReaderObserver
    {
        /// <summary>
        /// What to listen for
        /// </summary>
        DataReaderObserverTypes ReaderTypes { get; }

        /// <summary>
        /// The callback handler function
        /// </summary>
        /// <param name="pk">The primary key</param>
        /// <param name="fieldname">The column/field name</param>
        /// <param name="data">The data that was read</param>
        /// <param name="targetType">To what reader type it belongs to</param>
        void Handle(object pk, string fieldname, object data, DataReaderObserverTypes targetType);
    }
}

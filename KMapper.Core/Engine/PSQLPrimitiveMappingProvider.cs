﻿using KMapper.Core.Exceptions;
using KMapper.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Engine
{
    public class PSQLPrimitiveMappingProvider : IPrimitiveMappingProvider
    {
        // TODO: Maybe use internal from postgres engine?
        private static IDictionary<Type, string> InternalTypeMapping = new Dictionary<Type, string>
        {
            {typeof(string), "text" },
            {typeof(uint), "int" },
            {typeof(int), "int" },
            {typeof(bool), "bool" },
            {typeof(float), "real" },
            {typeof(double), "real" },
            {typeof(DateTime), "date" }
        };

        public string GetDbTypeAsString(Type type)
        {
            type = Nullable.GetUnderlyingType(type) ?? type;

            if (!InternalTypeMapping.TryGetValue(type, out string retVal))
                throw new MapperException($"Type {type.Name} is not registered as primitive type", type);

            return retVal;
        }
    }
}

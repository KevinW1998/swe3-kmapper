﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Engine
{
    internal class PSQLQueryBuilder
    {
        private StringBuilder internalBuilder = new StringBuilder();

        private int Counter = 0;

        public Dictionary<string, object> Parameters { get; } = new Dictionary<string, object>();

        public void Append(string text) => internalBuilder.Append(text);

        public void AppendLine(string text) => internalBuilder.AppendLine(text);

        public string Query => internalBuilder.ToString();

        public string AddCommonParameter(object para)
        {
            string paramName = $"__GenericParam_{Counter++}";
            Parameters.Add(paramName, para);
            return "@" + paramName;
        }
    }
}

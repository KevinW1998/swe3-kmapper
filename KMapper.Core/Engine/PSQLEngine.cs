﻿using KMapper.Core.Exceptions;
using KMapper.Core.Interfaces;
using KMapper.Core.Interfaces.Data;
using KMapper.Core.Interfaces.Enum;
using KMapper.Core.Metadata;
using Npgsql;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace KMapper.Core.Engine
{
    /// <summary>
    /// The main engine that communcates with postgres.
    /// </summary>
    public class PSQLEngine : IDisposable, IDataStorage
    {
        private NpgsqlConnection currentConnection = null;
        private PSQLConfiguration config;
        private IMetadataClassProvider metadataClassProvider;
        private IPrimitiveMappingProvider primitiveMappingProvider;
        private IList<string> preregisterdTableNames = new List<string>();

        #region Internal Structures
        private class InternalEngineSQLQueryData : ISQLQueryExtraData
        {
            public string WriteableWhereQuery { get; set; } = "";

            public Dictionary<string, object> WriteableParams { get; } = new Dictionary<string, object>();

            public List<MetadataClass> WriteableRequiredNaturalInnerJoinWith = new List<MetadataClass>();

            public List<string> WriteableExtraSelectFields = new List<string>();

            public List<ExtraDataInnerJoinDefinition> WriteableRequiredInnerJoinsWith = new List<ExtraDataInnerJoinDefinition>();

            public string WhereQuery => WriteableWhereQuery;

            public IReadOnlyDictionary<string, object> Parameters => WriteableParams;

            public IReadOnlyList<MetadataClass> RequiredNaturalInnerJoinWith => WriteableRequiredNaturalInnerJoinWith;

            public IReadOnlyList<string> OptionalExtraSelectFields => WriteableExtraSelectFields;

            public IReadOnlyList<ExtraDataInnerJoinDefinition> RequiredInnerJoinsWith => WriteableRequiredInnerJoinsWith;
        }
        #endregion

        #region Init
        /// <summary>
        /// Creates a new engine instance
        /// </summary>
        /// <param name="config">The config to use</param>
        /// <param name="metadataClassProvider">Metadata provider</param>
        /// <param name="primitiveMappingProvider">Mapping provider</param>
        public PSQLEngine(PSQLConfiguration config, IMetadataClassProvider metadataClassProvider, IPrimitiveMappingProvider primitiveMappingProvider)
        {
            this.config = config;
            this.metadataClassProvider = metadataClassProvider;
            this.primitiveMappingProvider = primitiveMappingProvider;
        }

        /// <summary>
        /// Reads the table names to see what types already have been registered.
        /// </summary>
        private void ReadTableNames()
        {
            preregisterdTableNames.Clear();

            using var cmd = new NpgsqlCommand("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' AND table_type = 'BASE TABLE'; ", currentConnection);
            using var reader = cmd.ExecuteReader();

            while (reader.Read())
                preregisterdTableNames.Add(reader.GetString(0));
        }

        /// <summary>
        /// Opens up the connection
        /// </summary>
        public void Open()
        {
            string connectionStr = $"Host={config.Host};Username={config.User};Password={config.Password};Database={config.Database};Include Error Detail={config.EnableDetailed}";

            currentConnection = new NpgsqlConnection(connectionStr);
            currentConnection.Open();

            ReadTableNames();
        }

        public bool IsOpen() => currentConnection != null;
        #endregion

        /// <summary>
        /// Checks if a type has been already registered
        /// </summary>
        /// <param name="name">The name of the type</param>
        /// <returns>Is registered</returns>
        private bool HasTableRegistered(string name)
        {
            return preregisterdTableNames.Contains(name.ToLower());
        }

        #region SQL-Scripts

        /// <summary>
        /// Creats a CREATE-Script for the type
        /// </summary>
        /// <param name="metadata">The metadata of the type</param>
        /// <returns>CREATE-Script</returns>
        private string GenerateCreateScript(MetadataClass metadata)
        {
            if (metadata.HasNoFields())
                throw new MapperException($"Class {metadata.ClassName} must have at least one property attribute", metadata.EnclosingType);

            StringBuilder builder = new StringBuilder();
            builder.Append($"CREATE TABLE {metadata.ClassName} (");

            List<string> sqlDataLines = new List<string>();
            foreach (MetadataClassProperty property in metadata.PrimitiveProperties)
            {
                StringBuilder lineBuilder = new StringBuilder();

                lineBuilder.Append($"{property.Name} {primitiveMappingProvider.GetDbTypeAsString(property.Type)}");
                if (property.IsNotNull)
                    lineBuilder.Append(" NOT NULL");
                if (property.IsUnique)
                    lineBuilder.Append(" UNIQUE");

                sqlDataLines.Add(lineBuilder.ToString());
            }

            // Define relations
            foreach (MetadataClassRelationProperty property in metadata.SimpleRelationProperties)
            {
                StringBuilder lineBuilder = new StringBuilder();

                lineBuilder.Append($"{property.Name} {primitiveMappingProvider.GetDbTypeAsString(property.RelatesTo.PrimaryKey.Type)}");
                if (property.IsNotNull)
                    lineBuilder.Append(" NOT NULL");
                if (property.IsUnique)
                    lineBuilder.Append(" UNIQUE");

                sqlDataLines.Add(lineBuilder.ToString());
            }

            // Define Primary Key
            if (metadata.IsPrimaryKeyAutoIncrement)
                sqlDataLines.Add($"{metadata.PrimaryKey.Name} serial");
            else
                sqlDataLines.Add($"{metadata.PrimaryKey.Name} {primitiveMappingProvider.GetDbTypeAsString(metadata.PrimaryKey.Type)}");
            sqlDataLines.Add($"PRIMARY KEY ({metadata.PrimaryKey.Name})");
            if(metadata.HasBaseType)
            {
                sqlDataLines.Add($"CONSTRAINT fk_{metadata.PrimaryKey.Name} " +
                    $"FOREIGN KEY({metadata.PrimaryKey.Name}) REFERENCES {metadata.BaseType.ClassName}({metadata.BaseType.PrimaryKey.Name}) " +
                    $"ON DELETE CASCADE");
            }

            // Define relation FK
            foreach (MetadataClassRelationProperty property in metadata.SimpleRelationProperties)
            {
                sqlDataLines.Add($"CONSTRAINT fk_{property.Name} " +
                    $"FOREIGN KEY({property.Name}) REFERENCES {property.RelatesTo.ClassName}({property.RelatesTo.PrimaryKey.Name}) " +
                    $"ON DELETE SET NULL");
            }

            builder.Append(string.Join(", ", sqlDataLines));
            builder.Append(");");

            // Define 1:N relation (using ALTER TABLE)
            foreach(MetadataClassListRelationProperty property in metadata.NRelationProperties)
            {
                builder.AppendLine($"ALTER TABLE {property.RelatesTo.ClassName} ADD COLUMN {property.ForeignKeyName} {primitiveMappingProvider.GetDbTypeAsString(metadata.PrimaryKey.Type)};");
                builder.AppendLine($"ALTER TABLE {property.RelatesTo.ClassName} ADD CONSTRAINT fk_{property.ForeignKeyName} FOREIGN KEY({property.ForeignKeyName}) REFERENCES {metadata.ClassName}({metadata.PrimaryKey.Name}) ON DELETE SET NULL;");
            }

            // Define M:N relation
            foreach(MetadataClassMToNRelationProperty property in metadata.MToNRelationProperties)
            {
                if(!property.IsResolved)
                {
                    builder.AppendLine($"CREATE TABLE {property.CommonTableName} " +
                        $"(mton_id serial, " +
                        $"{property.ForeignKeyName} {primitiveMappingProvider.GetDbTypeAsString(metadata.PrimaryKey.Type)}, " +
                        $"PRIMARY KEY (mton_id), " +
                        $"FOREIGN KEY({property.ForeignKeyName}) REFERENCES {metadata.ClassName}({metadata.PrimaryKey.Name}) ON DELETE CASCADE);");
                }
                else
                {
                    var otherSide = property.RelatesTo.FindMToNRelationPropertyByName(property.CommonTableName);

                    builder.AppendLine($"ALTER TABLE {property.CommonTableName} ADD COLUMN {property.ForeignKeyName} {primitiveMappingProvider.GetDbTypeAsString(metadata.PrimaryKey.Type)};");
                    builder.AppendLine($"ALTER TABLE {property.CommonTableName} ADD CONSTRAINT fk_{property.ForeignKeyName} FOREIGN KEY({property.ForeignKeyName}) REFERENCES {metadata.ClassName}({metadata.PrimaryKey.Name}) ON DELETE CASCADE;");
                    builder.AppendLine($"ALTER TABLE {property.CommonTableName} ADD CONSTRAINT uni_{otherSide.ForeignKeyName}_{property.ForeignKeyName} UNIQUE ({otherSide.ForeignKeyName}, {property.ForeignKeyName});");

                }
            }

            return builder.ToString();
        }

        /// <summary>
        /// Creates a prepared INSERT-Script
        /// </summary>
        /// <param name="builder">The query builder to append to</param>
        /// <param name="data">The data to insert</param>
        /// <param name="target">Target type (i.e. base type). Null to use type of the data</param>
        private void GeneratePreparedInsertScriptWithAutoIncrement(PSQLQueryBuilder builder, object data, MetadataClass target = null)
        {
            MetadataClass metadata = target ?? metadataClassProvider.Get(data.GetType());

            if (metadata.HasBaseType)
                GeneratePreparedInsertScriptWithAutoIncrement(builder, data, metadata.BaseType);

            var propNames = metadata.QueryProperties(MetadataClassQueryFlags.Primitive | MetadataClassQueryFlags.SimpleRelations).Select(x => x.Name);
            builder.Append($"INSERT INTO {metadata.ClassName} ({string.Join(", ", propNames)}");

            if(metadata.HasBaseType)
                builder.Append($", {metadata.PrimaryKey.Name}");
            builder.Append($") VALUES ({string.Join(", ", propNames.Select(x => "@" + x))}");

            if (metadata.HasBaseType)
                builder.Append($", lastval()");
            if (target == null)
                builder.Append($") RETURNING {metadata.PrimaryKey.Name};");
            else
                builder.Append(");");
        }

        #region Deprecated
        [Obsolete("Use GeneratePreparedUpsertScript instead")]
        private string GeneratePreparedInsertScript(object data, MetadataClass target = null)
        {
            MetadataClass metadata = target ?? metadataClassProvider.Get(data.GetType());

            string stmt = "";
            if (metadata.HasBaseType)
                stmt += GeneratePreparedInsertScript(data, metadata.BaseType);

            var propNames = metadata.QueryProperties(MetadataClassQueryFlags.Primitive | MetadataClassQueryFlags.SimpleRelations).Select(x => x.Name);
            stmt += $"INSERT INTO {metadata.ClassName} ({string.Join(", ", propNames)}";

            if (!metadata.IsPrimaryKeyAutoIncrement)
                stmt += $", {metadata.PrimaryKey.Name}";
            stmt += $") VALUES ({string.Join(", ", propNames.Select(x => "@" + x))}";

            if (!metadata.IsPrimaryKeyAutoIncrement)
                stmt += $", @{metadata.PrimaryKey.Name});";
            else if (target == null)
                stmt += $") RETURNING {metadata.PrimaryKey.Name};";
            else
                stmt += ");";

            return stmt;
        }

        [Obsolete("Use GeneratePreparedUpsertScript instead")]
        private string GeneratePreparedUpdateScript(object data, MetadataClass target = null)
        {
            MetadataClass metadata = target ?? metadataClassProvider.Get(data.GetType());

            string stmt = "";
            if (metadata.HasBaseType)
                stmt += GeneratePreparedUpdateScript(data, metadata.BaseType);

            var propNames = metadata.QueryProperties(MetadataClassQueryFlags.Primitive | MetadataClassQueryFlags.SimpleRelations).Select(x => x.Name);
            stmt += $"UPDATE {metadata.ClassName} SET {string.Join(", ", propNames.Select(x => x + " = @" + x))} WHERE {metadata.PrimaryKey.Name} = @{metadata.PrimaryKey.Name};";

            return stmt;
        }
        #endregion

        /// <summary>
        /// Creates a script to update relation data.
        /// </summary>
        /// <param name="builder">The query to append to</param>
        /// <param name="data">The data to update the relations</param>
        private void GenerateUpdateRelationScript(PSQLQueryBuilder builder, object data)
        {
            MetadataClass metadata = metadataClassProvider.Get(data.GetType());
            var listRelations = metadata.QueryProperties(MetadataClassQueryFlags.OneToNRelations | MetadataClassQueryFlags.Recurse).Cast<MetadataClassListRelationProperty>();
            var mToNRelations = metadata.QueryProperties(MetadataClassQueryFlags.MToNRelations | MetadataClassQueryFlags.Recurse).Cast<MetadataClassMToNRelationProperty>();
            var dataPk = metadata.PrimaryKey.PropertyInfo.GetValue(data);
            var dataPkPrepared = builder.AddCommonParameter(dataPk);

            // 1:N relations
            foreach (MetadataClassListRelationProperty listRelationProperty in listRelations)
            {
                var foreignKeyPropertyInfo = listRelationProperty.RelatesTo.PrimaryKey.PropertyInfo;
                var mainPrimaryKey = metadata.PrimaryKey.PropertyInfo.GetValue(data);
                var mainPrimaryKeyPrepared = builder.AddCommonParameter(mainPrimaryKey);
                var listProperty = listRelationProperty.PropertyInfo.GetValue(data) as IEnumerable;
                var ids = listProperty.Cast<object>().Select(x => foreignKeyPropertyInfo.GetValue(x));
                if (ids.Count() <= 0)
                    continue;

                var relatesToPrimaryKeyName = listRelationProperty.RelatesTo.PrimaryKey.Name;

                // source: https://stackoverflow.com/a/18799497
                builder.Append($"UPDATE {listRelationProperty.RelatesTo.ClassName} AS t SET ");
                builder.Append($"{listRelationProperty.ForeignKeyName} = c.{listRelationProperty.ForeignKeyName} ");
                builder.Append($"FROM (values ");

                builder.Append(string.Join(",", ids.Select(currentFK => $"({builder.AddCommonParameter(currentFK)}, {mainPrimaryKeyPrepared})")));

                builder.Append($") AS c ({relatesToPrimaryKeyName}, {listRelationProperty.ForeignKeyName}) ");
                builder.Append($"WHERE c.{relatesToPrimaryKeyName} = t.{relatesToPrimaryKeyName};");

                // TODO: Set null for values that doesn't exists anymore?
            }

            // N:M relations
            foreach (MetadataClassMToNRelationProperty mToNRelationProperty in mToNRelations)
            {
                var otherSide = mToNRelationProperty.RelatesTo.FindMToNRelationPropertyByName(mToNRelationProperty.CommonTableName);
                var foreignKeyPropertyInfo = mToNRelationProperty.RelatesTo.PrimaryKey.PropertyInfo;
                var listProperty = mToNRelationProperty.PropertyInfo.GetValue(data) as IEnumerable;
                var ids = listProperty.Cast<object>().Select(x => foreignKeyPropertyInfo.GetValue(x));
                if (ids.Count() <= 0)
                    continue;

                // First delete all that are not in list:
                builder.Append($"DELETE FROM {mToNRelationProperty.CommonTableName} WHERE {mToNRelationProperty.ForeignKeyName} = {dataPkPrepared}");
                if (ids.Count() > 0)
                    builder.Append($" AND {otherSide.ForeignKeyName} NOT IN ({string.Join(",", ids.Select(id => builder.AddCommonParameter(id)))})");
                builder.Append(";");

                // Now insert with conflict do nothing
                if (ids.Count() > 0)
                {
                    builder.Append($"INSERT INTO {mToNRelationProperty.CommonTableName} ({mToNRelationProperty.ForeignKeyName}, {otherSide.ForeignKeyName}) VALUES ");
                    builder.Append(string.Join(",", ids.Select(x => $"({dataPkPrepared}, {builder.AddCommonParameter(x)})")));
                    builder.Append(" ON CONFLICT DO NOTHING;");
                }
            }
        }

        /// <summary>
        /// Creates a UPSERT (Insert or Update)-Script.
        /// </summary>
        /// <param name="builder">The query to append to</param>
        /// <param name="data">The data to upsert</param>
        /// <param name="target">Target type (i.e. base type). Null to use type of the data</param>
        private void GeneratePreparedUpsertScript(PSQLQueryBuilder builder, object data, MetadataClass target = null)
        {
            MetadataClass metadata = target ?? metadataClassProvider.Get(data.GetType());

            // For auto increment primary key add custom logic
            if(metadata.IsPrimaryKeyAutoIncrement)
            {
                var primary = metadata.PrimaryKey.PropertyInfo.GetValue(data);
                if(primary == null)
                {
                    GeneratePreparedInsertScriptWithAutoIncrement(builder, data);
                    return;
                    // return GeneratePreparedInsertScript(data);
                }
            }

            if (metadata.HasBaseType)
                GeneratePreparedUpsertScript(builder, data, metadata.BaseType);

            var propNames = metadata.QueryProperties(MetadataClassQueryFlags.Primitive | MetadataClassQueryFlags.SimpleRelations).Select(x => x.Name);
            var propNamesWithPK = propNames.Append(metadata.PrimaryKey.Name);

            builder.Append($"INSERT INTO {metadata.ClassName} ({string.Join(", ", propNamesWithPK)}) VALUES ("
                + string.Join(", ", propNamesWithPK.Select(x => "@" + x)) +
                $")");

            if(propNames.Count() > 0)
                builder.Append($" ON CONFLICT({ metadata.PrimaryKey.Name}) DO UPDATE SET " +
                $"{string.Join(", ", propNames.Select(x => x + " = EXCLUDED." + x))};");

            builder.Append(";");

        }

        /// <summary>
        /// Creates a complete UPSERT-Script (including relations)
        /// </summary>
        /// <param name="builder">The query to append to</param>
        /// <param name="data">The data to upsert</param>
        private void GenerateCompletePreparedUpsertScript(PSQLQueryBuilder builder, object data)
        {
            GeneratePreparedUpsertScript(builder, data);
            GenerateUpdateRelationScript(builder, data);
        }

        /// <summary>
        /// Creates a SELECT-Script
        /// </summary>
        /// <param name="metadata">The metadata of the target type</param>
        /// <param name="where">The extra data containing filters if provided</param>
        /// <param name="joinBases">True if base types shall be joined for better performance</param>
        /// <returns>The complete SQL script</returns>
        private string GenerateSelectAllScript(MetadataClass metadata, ISQLQueryExtraData where, bool joinBases = false)
        {
            StringBuilder stmtBuilder = new StringBuilder();

            MetadataClassQueryFlags queryFlags = MetadataClassQueryFlags.Primitive | MetadataClassQueryFlags.PrimaryKey;
            if (joinBases)
                queryFlags |= MetadataClassQueryFlags.Recurse;

            var props = metadata.QueryProperties(queryFlags)
                .Select(x => (!joinBases && x.IsPrimaryKey) ? $"{metadata.ClassName}.{x.Name}" : $"{x.Parent.ClassName}.{x.Name}"); // If we are not aware of our base class, then use id from current class

            if (where != null && where.OptionalExtraSelectFields.Count > 0)
                props = props.Concat(where.OptionalExtraSelectFields);
            
            stmtBuilder.Append($"SELECT {string.Join(", ", props)} " +
                $"FROM {metadata.ClassName}");

            if(joinBases || where != null)
            {
                MetadataClass baseType = metadata.BaseType;
                while (baseType != null)
                {
                    stmtBuilder.Append($" NATURAL INNER JOIN {baseType.ClassName}");
                    baseType = baseType.BaseType;
                }
            }
            
            if(where != null)
            {
                foreach (var refReqMetadata in where.RequiredNaturalInnerJoinWith)
                    stmtBuilder.Append($" NATURAL INNER JOIN {refReqMetadata.ClassName}");
                foreach (var refReqMetadata in where.RequiredInnerJoinsWith)
                    stmtBuilder.Append($" INNER JOIN {refReqMetadata.ForeignTable} ON {metadata.ClassName}.{refReqMetadata.OwnTableField} = {refReqMetadata.ForeignTable}.{refReqMetadata.ForeignTableField}");

                string wherePart = where.WhereQuery;
                if (!wherePart.StartsWith(" "))
                    wherePart = " " + wherePart;

                stmtBuilder.Append(" WHERE");
                stmtBuilder.Append(wherePart);
            }

            stmtBuilder.Append(";");

            return stmtBuilder.ToString();
        }

        /// <summary>
        /// Creates a DELETE-Script of the given type. Deletes the root base type, as the inheritence relation is set on CASCADE DELETE.
        /// </summary>
        /// <param name="metadata">The metadata of the type</param>
        /// <returns>The DELETE-Script</returns>
        private string GenerateDeleteScript(MetadataClass metadata)
        {
            // Remove root base, because of on delete cascade!
            MetadataClass rootBaseType = metadata.GetRootBaseType();

            StringBuilder stmtBuilder = new StringBuilder();
            stmtBuilder.Append($"DELETE FROM {rootBaseType.ClassName} WHERE {rootBaseType.PrimaryKey.Name} = @{rootBaseType.PrimaryKey.Name}");

            return stmtBuilder.ToString();
        }

        #endregion

        /// <summary>
        /// Executes a simple SQL script (with no results)
        /// </summary>
        /// <param name="sql">The given SQL script</param>
        public void ExecuteSimple(string sql)
        {
            using var cmd = new NpgsqlCommand(sql, currentConnection);

            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Registeres a new type to the DB-Side if it does not yet exist.
        /// </summary>
        /// <param name="metadataClass">The metadata of the type</param>
        public void Register(MetadataClass metadataClass)
        {
            if(!HasTableRegistered(metadataClass.ClassName))
                ExecuteSimple(GenerateCreateScript(metadataClass));
        }

        /// <summary>
        /// Upserts the data
        /// </summary>
        /// <param name="data">The data to upsert</param>
        public void Upsert(object data)
        {
            PSQLQueryBuilder queryBuilder = new PSQLQueryBuilder();

            GenerateCompletePreparedUpsertScript(queryBuilder, data);

            using var cmd = new NpgsqlCommand(queryBuilder.Query, currentConnection);
            MetadataClass metadata = metadataClassProvider.Get(data.GetType());

            AddParametersWithValue(queryBuilder, data, cmd, metadata);

            // Check if we can extract new primary key
            if(metadata.IsPrimaryKeyAutoIncrement)
            {
                var pkPropInfo = metadata.PrimaryKey.PropertyInfo;
                var pk = pkPropInfo.GetValue(data);
                if(pk == null)
                {
                    using var reader = cmd.ExecuteReader();
                    // Ensure valid
                    if (!reader.Read())
                        throw new MapperException("Failed to read new auto increment primary key!", metadata.EnclosingType);

                    if (reader.GetName(0) != metadata.PrimaryKey.Name)
                        throw new MapperException("First entry of query is not primary key!", metadata.EnclosingType);

                    pkPropInfo.SetValue(data, reader.GetValue(0));
                    return;
                }
            }

            // Insert primary key
            cmd.Parameters.AddWithValue(metadata.PrimaryKey.Name, metadata.PrimaryKey.PropertyInfo.GetValue(data));

            // Otherwise default non-query
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Fills in the prepared statement with data
        /// </summary>
        /// <param name="builder">The current query</param>
        /// <param name="data">The data</param>
        /// <param name="cmd">The PSQL command to fill in the data</param>
        /// <param name="metadata">The target type metadata</param>
        private void AddParametersWithValue(PSQLQueryBuilder builder, object data, NpgsqlCommand cmd, MetadataClass metadata)
        {
            if (metadata.HasBaseType)
            {
                AddParametersWithValue(builder, data, cmd, metadata.BaseType);
            }
            else
            {
                // Fill in extra parameters from query builder
                foreach (var paramPairs in builder.Parameters)
                    cmd.Parameters.AddWithValue(paramPairs.Key, paramPairs.Value ?? DBNull.Value);
            }

            // Insert parameters
            foreach (MetadataClassProperty prop in metadata.PrimitiveProperties)
            {
                object propData = prop.PropertyInfo.GetValue(data);

                cmd.Parameters.AddWithValue(prop.Name, propData ?? DBNull.Value);
            }

            // Insert relation parameters
            foreach (MetadataClassRelationProperty prop in metadata.SimpleRelationProperties)
            {
                object propData = prop.PropertyInfo.GetValue(data);

                object pk = propData != null ? prop.RelatesTo.PrimaryKey.PropertyInfo.GetValue(propData) : null;

                cmd.Parameters.AddWithValue(prop.Name, pk ?? DBNull.Value);
            }
        }

        /// <summary>
        /// (Recursively) collects the data of a given type. It starts with the target type and first collects the data part of derived instances.
        /// </summary>
        /// <typeparam name="T">The target type</typeparam>
        /// <param name="gatheredObjects">Gathered objects so far</param>
        /// <param name="metadataClass">The metadata of the target type</param>
        /// <param name="where">The where filter</param>
        /// <param name="optObserver">An optional data observer that notifies when data is read</param>
        /// <param name="isRoot">True, if it is on the root of the recursive function</param>
        private void CollectDerivedObjects<T>(IDictionary<object, T> gatheredObjects, MetadataClass metadataClass, ISQLQueryExtraData where = null, IDataReaderObserver optObserver = null, bool isRoot = true)
        {
            foreach(MetadataClass derivedMetadataClass in metadataClass.DerivedTypes)
            {
                CollectDerivedObjects(gatheredObjects, derivedMetadataClass, where, optObserver, false);
            }

            using var cmd = new NpgsqlCommand(GenerateSelectAllScript(metadataClass, where, isRoot), currentConnection);
            
            if(where != null)
            {
                foreach (KeyValuePair<string, object> param in where.Parameters)
                    cmd.Parameters.AddWithValue(param.Key, param.Value ?? DBNull.Value);
            }
            
            
            using var reader = cmd.ExecuteReader();

            // Map ordinal --> MetadataClassProperty for performance
            int primaryKeyOrdinalIdx = -1;
            List<MetadataClassProperty> mappedColIdToProp = new List<MetadataClassProperty>();
            for (int i = 0; i < reader.FieldCount; i++)
            {
                string fieldName = reader.GetName(i);

                var prop = metadataClass.TryFindPropertyRecurse(fieldName);
                if (prop == null)
                {
                    // throw new MapperException($"Failed to find matching property in {metadataClass.EnclosingType.Name} to field {fieldName}", metadataClass.EnclosingType);
                    mappedColIdToProp.Add(null);
                    continue;
                }

                if (prop.IsPrimaryKey)
                    primaryKeyOrdinalIdx = i;

                mappedColIdToProp.Add(prop);
            }

            while (reader.Read())
            {
                // T newInstance = Activator.CreateInstance<T>();
                // First get primary key
                object pk = reader.GetValue(primaryKeyOrdinalIdx);
                if(!gatheredObjects.TryGetValue(pk, out T objToWrite))
                {
                    objToWrite = (T)Activator.CreateInstance(metadataClass.EnclosingType);
                    mappedColIdToProp[primaryKeyOrdinalIdx].PropertyInfo.SetValue(objToWrite, pk);
                    gatheredObjects.Add(pk, objToWrite);
                }

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    MetadataClassProperty prop = mappedColIdToProp[i];
                    if (prop == null) {
                        if (optObserver != null && optObserver.ReaderTypes.HasFlag(DataReaderObserverTypes.UnknownFields))
                        {
                            object unknownFieldValue = reader.GetValue(i);
                            if (unknownFieldValue is DBNull)
                                unknownFieldValue = null;

                            optObserver.Handle(pk, reader.GetName(i), unknownFieldValue, DataReaderObserverTypes.UnknownFields);
                        }

                        continue;
                    } // skip unidentified fields
                    
                    if (prop.IsPrimaryKey)
                        continue;

                    object valueOut = reader.GetValue(i);
                    if (valueOut is DBNull)
                        valueOut = null;

                    if(optObserver != null && optObserver.ReaderTypes.HasFlag(DataReaderObserverTypes.MappedFields))
                    {
                        optObserver.Handle(pk, reader.GetName(i), valueOut, DataReaderObserverTypes.MappedFields);
                    }
                    prop.PropertyInfo.SetValue(objToWrite, valueOut);
                }
            }
        }

        public void Delete(object data)
        {
            MetadataClass metadata = metadataClassProvider.Get(data.GetType());

            using var cmd = new NpgsqlCommand(GenerateDeleteScript(metadata), currentConnection);

            // Insert primary key
            cmd.Parameters.AddWithValue(metadata.PrimaryKey.Name, metadata.PrimaryKey.PropertyInfo.GetValue(data));

            cmd.ExecuteNonQuery();
        }

        
        /// <summary>
        /// Resolves and reads relation data
        /// </summary>
        /// <typeparam name="T">The given target types</typeparam>
        /// <param name="dataObjects">The data objects to update the relations data</param>
        /// <param name="metadata">The given metadata</param>
        /// <param name="resolveMToN">If M:N relations shall be resolved (to prevent infinite recursions)</param>
        private void ResolveRelations<T>(ICollection<T> dataObjects, MetadataClass metadata, bool resolveMToN = true)
        {
            if (dataObjects.Count < 1)
                return;

            // Resolve 1:1
            foreach(var simpleRelation in metadata.QueryProperties(MetadataClassQueryFlags.Recurse | MetadataClassQueryFlags.SimpleRelations).Cast<MetadataClassRelationProperty>())
            {
                InternalEngineSQLQueryData idFilter = new InternalEngineSQLQueryData();
                
                // Now filter those with valid ids and map them to a dict
                Dictionary<object, object> objectMappedToIds = new Dictionary<object, object>();
                foreach(T data in dataObjects)
                {
                    object idObj = metadata.PrimaryKey.PropertyInfo.GetValue(data);
                    objectMappedToIds.Add(idObj, data);
                }

                // Setup our where
                string fkRename = $"{metadata.ClassName}_{simpleRelation.RelatesTo.PrimaryKey.Name}";
                idFilter.WriteableWhereQuery = $"{metadata.ClassName}.{simpleRelation.RelatesTo.PrimaryKey.Name} IN ({string.Join(", ", objectMappedToIds.Keys)})";
                idFilter.WriteableRequiredNaturalInnerJoinWith.Add(metadata);
                idFilter.WriteableExtraSelectFields.Add($"{metadata.ClassName}.{simpleRelation.RelatesTo.PrimaryKey.Name} AS {fkRename}");

                PSQLUnknownDataCollector dataCollector = new PSQLUnknownDataCollector();

                IList<object> fkObj = GetWhere<object>(idFilter, null, simpleRelation.RelatesTo, dataCollector);
                foreach(var obj in fkObj)
                {
                    var fkPk = simpleRelation.RelatesTo.PrimaryKey.PropertyInfo.GetValue(obj);
                    var mainObjPk = dataCollector.GetValue(fkPk, fkRename);
                    var mainObj = objectMappedToIds[mainObjPk];

                    simpleRelation.PropertyInfo.SetValue(mainObj, obj);
                }
            }

            // Resolve 1:N
            foreach(var listRelation in metadata.QueryProperties(MetadataClassQueryFlags.Recurse | MetadataClassQueryFlags.OneToNRelations).Cast<MetadataClassListRelationProperty>())
            {
                InternalEngineSQLQueryData idFilter = new InternalEngineSQLQueryData();

                idFilter.WriteableWhereQuery = $"{listRelation.RelatesTo.ClassName}.{listRelation.ForeignKeyName} = @target_id";
                idFilter.WriteableRequiredInnerJoinsWith.Add(new ExtraDataInnerJoinDefinition()
                {
                    ForeignTable = metadata.ClassName,
                    ForeignTableField = metadata.PrimaryKey.Name,
                    OwnTableField = listRelation.ForeignKeyName
                });

                foreach(T data in dataObjects)
                {
                    var pk = metadata.PrimaryKey.PropertyInfo.GetValue(data);
                    idFilter.WriteableParams["target_id"] = pk;

                    var listObj = GetWhere<object>(idFilter, null, listRelation.RelatesTo);
                    
                    // Create type-specific list [See https://stackoverflow.com/a/11965991]
                    var listType = typeof(List<>).MakeGenericType(new Type[] { listRelation.ContainerType });
                    var targetList = (IList)Activator.CreateInstance(listType);
                    foreach (var obj in listObj) // TODO: Find better solution?
                        targetList.Add(obj);

                    listRelation.PropertyInfo.SetValue(data, targetList);
                }
            }

            if(resolveMToN)
            {
                // Resolve N:M
                foreach (var mToNRelation in metadata.QueryProperties(MetadataClassQueryFlags.Recurse | MetadataClassQueryFlags.MToNRelations).Cast<MetadataClassMToNRelationProperty>())
                {
                    var otherSideMetadata = mToNRelation.RelatesTo;
                    var otherSideRelation = mToNRelation.RelatesTo.FindMToNRelationPropertyByName(mToNRelation.CommonTableName);

                    InternalEngineSQLQueryData idFilter = new InternalEngineSQLQueryData();

                    idFilter.WriteableWhereQuery = $"{mToNRelation.CommonTableName}.{mToNRelation.ForeignKeyName} = @target_id";
                    idFilter.WriteableRequiredInnerJoinsWith.Add(new ExtraDataInnerJoinDefinition()
                    {
                        ForeignTable = mToNRelation.CommonTableName,
                        ForeignTableField = otherSideRelation.ForeignKeyName,
                        OwnTableField = otherSideMetadata.PrimaryKey.Name
                    });

                    List<(object pkOwn, object pkForeign, object own)> mappedData = new List<(object pkOwn, object pkForeign, object own)>();

                    foreach (T data in dataObjects)
                    {
                        var pk = metadata.PrimaryKey.PropertyInfo.GetValue(data);
                        idFilter.WriteableParams["target_id"] = pk;

                        var listObj = GetWhere<object>(idFilter, null, otherSideMetadata, null, PSQLEngineQueryFlags.NoMToNRelation);

                        // Create type-specific list [See https://stackoverflow.com/a/11965991]
                        var listType = typeof(List<>).MakeGenericType(new Type[] { mToNRelation.ContainerType });
                        var targetList = (IList)Activator.CreateInstance(listType);
                        foreach (var obj in listObj)
                        {
                            targetList.Add(obj);

                            var pkForeign = otherSideMetadata.PrimaryKey.PropertyInfo.GetValue(obj);
                            mappedData.Add((pk, pkForeign, data));
                        }

                        mToNRelation.PropertyInfo.SetValue(data, targetList);
                    }

                    // Now resolve the other side
                    foreach(T data in dataObjects)
                    {
                        var pk = metadata.PrimaryKey.PropertyInfo.GetValue(data);

                        var targetList = (IList)mToNRelation.PropertyInfo.GetValue(data);
                        foreach(object otherSideObject in targetList)
                        {
                            var otherSideTargetList = (IList)otherSideRelation.PropertyInfo.GetValue(otherSideObject);
                            var otherSidePK = otherSideMetadata.PrimaryKey.PropertyInfo.GetValue(otherSideObject);

                            var entries = mappedData.Where(x => x.pkOwn.Equals(pk) && x.pkForeign.Equals(otherSidePK)).Select(x => x.own);
                            foreach(var entry in entries)
                                otherSideTargetList.Add(entry);
                        }
                    }
                }
            }
        }

        public IList<T> GetAll<T>()
        {
            MetadataClass metadata = metadataClassProvider.Get(typeof(T));

            return GetAll<T>(metadata);
        }

        /// <summary>
        /// Same as GetAll, but with observer
        /// </summary>
        private IList<T> GetAll<T>(MetadataClass targetType, IDataReaderObserver observer = null)
        {
            IDictionary<object, T> gatheredDerivedObjects = new Dictionary<object, T>();

            CollectDerivedObjects(gatheredDerivedObjects, targetType, null, observer);

            if (targetType.HasRelations())
                ResolveRelations(gatheredDerivedObjects.Values, targetType);

            return new List<T>(gatheredDerivedObjects.Values);
        }

        public IList<T> GetWhere<T>(ISQLQueryExtraData parsedWhere, Expression<Func<T, bool>> origWhereExpression)
        {
            MetadataClass metadata = metadataClassProvider.Get(typeof(T));

            return GetWhere(parsedWhere, origWhereExpression, metadata, null);
        }

        /// <summary>
        /// Same as GetWhere but with observer and query flags
        /// </summary>
        private IList<T> GetWhere<T>(ISQLQueryExtraData parsedWhere, Expression<Func<T, bool>> origWhereExpression, MetadataClass targetType, IDataReaderObserver observer = null, PSQLEngineQueryFlags flags = PSQLEngineQueryFlags.None)
        {
            IDictionary<object, T> gatheredDerivedObjects = new Dictionary<object, T>();

            CollectDerivedObjects(gatheredDerivedObjects, targetType, parsedWhere, observer);

            if (targetType.HasRelations())
                ResolveRelations(gatheredDerivedObjects.Values, targetType, !flags.HasFlag(PSQLEngineQueryFlags.NoMToNRelation));

            return new List<T>(gatheredDerivedObjects.Values);
        }

        public void Dispose() => currentConnection?.Dispose();

        public void BeginTransaction()
        {
            ExecuteSimple("BEGIN;");
        }

        public void Commit()
        {
            ExecuteSimple("COMMIT;");
        }

        public void Rollback()
        {
            ExecuteSimple("ROLLBACK;");
        }
    }
}

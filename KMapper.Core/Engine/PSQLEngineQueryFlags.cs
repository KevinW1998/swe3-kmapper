﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Engine
{
    [Flags]
    enum PSQLEngineQueryFlags
    {
        None = 0,
        NoMToNRelation = 1
    }
}

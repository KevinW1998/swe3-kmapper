﻿using KMapper.Core.Interfaces;
using KMapper.Core.Interfaces.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Engine
{
    internal class PSQLUnknownDataCollector : IDataReaderObserver
    {
        public DataReaderObserverTypes ReaderTypes => DataReaderObserverTypes.UnknownFields;

        private Dictionary<object, Dictionary<string, object>> unknownMappedFields = new Dictionary<object, Dictionary<string, object>>();

        public void Handle(object pk, string fieldname, object data, DataReaderObserverTypes targetType)
        {
            if(!unknownMappedFields.TryGetValue(pk, out var fieldMapper))
            {
                fieldMapper = new Dictionary<string, object>();
                unknownMappedFields.Add(pk, fieldMapper);
            }

            fieldMapper.Add(fieldname, data);
        }

        public object GetValue(object pk, string field)
        {
            return unknownMappedFields[pk][field];
        }
    }
}

﻿using KMapper.Core.Interfaces;
using KMapper.Core.Interfaces.Data;
using KMapper.Core.Metadata;
using KMapper.Core.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;

namespace KMapper.Core.Query
{
    public class PSQLScriptGenerator : IScriptGenerator
    {
        private class ScriptGeneratorQueryData : ISQLQueryExtraData
        {
            private int counter = 0;

            public StringBuilder Builder { get; } = new StringBuilder();

            public Dictionary<string, object> WriteableParameters = new Dictionary<string, object>();

            // TODO: Fill later
            private List<MetadataClass> ReferenceDummy = new List<MetadataClass>();

            private List<string> ReferenceDummy2 = new List<string>();

            private List<ExtraDataInnerJoinDefinition> ReferenceDummy3 = new List<ExtraDataInnerJoinDefinition>();

            public void AppendParam(object data)
            {
                string newParamName = "__GenericParam_" + counter++;
                Builder.Append("@");
                Builder.Append(newParamName);
                WriteableParameters.Add(newParamName, data);
            }

            public string WhereQuery => Builder.ToString();

            public IReadOnlyDictionary<string, object> Parameters => new ReadOnlyDictionary<string, object>(WriteableParameters);

            public IReadOnlyList<MetadataClass> RequiredNaturalInnerJoinWith => ReferenceDummy;

            public IReadOnlyList<string> OptionalExtraSelectFields => ReferenceDummy2;

            public IReadOnlyList<ExtraDataInnerJoinDefinition> RequiredInnerJoinsWith => ReferenceDummy3;
        }

        private static IDictionary<ExpressionType, string> expressionTypeMap = new Dictionary<ExpressionType, string>()
        {
            {ExpressionType.GreaterThan, ">"},
            {ExpressionType.GreaterThanOrEqual, ">=" },
            {ExpressionType.LessThan, "<" },
            {ExpressionType.LessThanOrEqual, "<=" },
            {ExpressionType.Equal, "=" },
            {ExpressionType.NotEqual, "!=" },
            {ExpressionType.AndAlso, "AND" },
            {ExpressionType.OrElse, "OR" }
        };

        private IMetadataClassProvider metadataClassProvider;

        public PSQLScriptGenerator(IMetadataClassProvider metadataClassProvider)
        {
            this.metadataClassProvider = metadataClassProvider;
        }

        private void WriteConstantObjectToQuery(ScriptGeneratorQueryData query, object obj)
        {
            query.Builder.Append(" ");
            // Special case: it seems that psql doesn't like to have null inserted as a prepared parameter with the IS operator.
            // This is why we inject it directly instead of using parameters
            if (obj == null)
                query.Builder.Append("NULL");
            else
                query.AppendParam(obj);
        }

        private void ParseExpression<T>(ScriptGeneratorQueryData query, Expression currentExpression)
        {
            switch (currentExpression)
            {
                case BinaryExpression binaryExpression:

                    query.Builder.Append("(");

                    // Parse Left
                    ParseExpression<T>(query, binaryExpression.Left);

                    // Emit Node Type
                    if (!expressionTypeMap.TryGetValue(binaryExpression.NodeType, out string token))
                        throw new NotSupportedException($"Node type {binaryExpression.NodeType} with BinaryExpression is not supported!");

                    // if(binaryExpression.NodeType == ExpressionType.Equal || binaryExpression.NodeType == ExpressionType.NotEqual)
                    // Special case null: 
                    ConstantExpression rightOptConstantExpression = binaryExpression.Right as ConstantExpression;
                    if(rightOptConstantExpression != null && rightOptConstantExpression.Value == null) 
                    {
                        if (binaryExpression.NodeType == ExpressionType.Equal)
                            token = "IS";
                        else if (binaryExpression.NodeType == ExpressionType.NotEqual)
                            token = "IS NOT";
                        else
                            throw new NotSupportedException("Other operators to compare with null is not supported!");
                    } 
                    
                    // SPECIAL CASE NULL:
                    query.Builder.Append(" ");
                    query.Builder.Append(token);

                    // Parse Right
                    ParseExpression<T>(query, binaryExpression.Right);

                    query.Builder.Append(")");

                    break;
                case MemberExpression memberExpression:

                    if(memberExpression.Expression is ConstantExpression)
                    {
                        ConstantExpression constantExpression = (ConstantExpression)memberExpression.Expression;
                        var obj = ReflectionUtils.GetValue(memberExpression.Member, constantExpression.Value);

                        WriteConstantObjectToQuery(query, obj);
                        break;
                    }

                    ParameterExpression memberParamExpression = memberExpression.Expression as ParameterExpression;
                    if (memberParamExpression == null)
                        throw new NotSupportedException("Member expression must have parameter expression");

                    if (memberParamExpression.Type != typeof(T))
                        throw new NotSupportedException("Parameter must be from the query type");

                    // memberExpression.Member
                    MetadataClass metadata = metadataClassProvider.Get(typeof(T));

                    var propData = metadata
                        .QueryProperties(MetadataClassQueryFlags.Recurse | MetadataClassQueryFlags.Primitive | MetadataClassQueryFlags.PrimaryKey)
                        .Where(p => p.PropertyInfo == memberExpression.Member)
                        .FirstOrDefault();

                    if (propData == null)
                        throw new NotSupportedException("Failed to get property member");

                    query.Builder.Append(" ");
                    query.Builder.Append(propData.Name);


                    break;

                case UnaryExpression unaryExpression:

                    if (unaryExpression.IsLiftedToNull)
                        ParseExpression<T>(query, unaryExpression.Operand);
                    else
                        throw new NotSupportedException("Other unary operators are not supported right now");

                    break;
                case ConstantExpression constantExpression:
                    WriteConstantObjectToQuery(query, constantExpression.Value);

                    break;
                default:
                    throw new NotSupportedException($"Expression type {currentExpression.GetType().Name} is not supported!");
            }
        }

        public ISQLQueryExtraData CreateQueryFromExpression<T>(Expression<Func<T, bool>> whereClause)
        {
            ScriptGeneratorQueryData query = new ScriptGeneratorQueryData();

            ParseExpression<T>(query, whereClause.Body);

            return query;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Attributes
{
    /// <summary>
    /// Marks an field as an M:N relation. This attribute can be only used with a List of the target object.
    /// The target object must also have a field with a M:N relation attribute that has the same common name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MToN : Attribute
    {
        public string CommonName { get; }

        public MToN(string commonName) {
            CommonName = commonName;
        }
    }
}

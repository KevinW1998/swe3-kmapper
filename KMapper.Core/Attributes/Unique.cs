﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Attributes
{
    /// <summary>
    /// Marks an element as unique.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class Unique : Attribute
    {
    }
}

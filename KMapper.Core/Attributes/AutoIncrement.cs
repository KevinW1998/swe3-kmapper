﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Attributes
{
    /// <summary>
    /// Marks an field as an auto-increment primary key. This field can only be used in conjunction with a PrimaryKey-Attribute
    /// </summary>
    /// <seealso cref="PrimaryKey"/>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class AutoIncrement : Attribute
    {
    }
}

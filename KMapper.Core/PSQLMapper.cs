﻿using KMapper.Core.Cache;
using KMapper.Core.Engine;
using KMapper.Core.Exceptions;
using KMapper.Core.Interfaces;
using KMapper.Core.Metadata;
using KMapper.Core.Query;
using KMapper.Core.Tracker;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace KMapper.Core
{
    /// <summary>
    /// The main class that provides the mapping API
    /// </summary>
    public class PSQLMapper : IDisposable
    {
        /// <summary>
        /// The engine powering the mapper. This engine runs the actual SQL scripts.
        /// </summary>
        private PSQLEngine engine;

        /// <summary>
        /// The mapper manager containing the metadata needed.
        /// </summary>
        private MetadataClassManager metadataClassManager = new MetadataClassManager();
        
        /// <summary>
        /// Mapping provider for primitive types
        /// </summary>
        private PSQLPrimitiveMappingProvider primitiveMappingProvider = new PSQLPrimitiveMappingProvider();

        /// <summary>
        /// The data storage interface. Can be either the cache or the engine itself.
        /// </summary>
        private IDataStorage targetStorage;

        /// <summary>
        /// Data state tracker (unused)
        /// </summary>
        private IDataStateTracker dataTracker;

        /// <summary>
        /// Script generator provider that can create an SQL script from an expression
        /// </summary>
        private IScriptGenerator scriptGenerator;


        /// <summary>
        /// Creates a new Mapper instance. The connection is not setup until "open" is called.
        /// </summary>
        /// <param name="configuration">The configuration to use</param>
        public PSQLMapper(PSQLConfiguration configuration)
        {
            engine = new PSQLEngine(configuration, metadataClassManager, primitiveMappingProvider);
            targetStorage = engine;
            dataTracker = new DataStateTrackerManager(metadataClassManager);
            scriptGenerator = new PSQLScriptGenerator(metadataClassManager);
        }

        /// <summary>
        /// Enables Caching. Once caching is enabled you cannot deactivate it.
        /// </summary>
        public void EnableCaching()
        {
            // The targetStorage is applied like the "Decorator"-Pattern. The cache provides the same operation
            // as the database but can redirect it to the engine if the cache doesn't provide the data that is needed.
            if (targetStorage == engine)
                targetStorage = new SimpleCacheStorage(engine, metadataClassManager);
        }

        /// <summary>
        /// Registeres a class to the DB-Side by generic type. Base types are automatically registered aswell.
        /// </summary>
        /// <typeparam name="T">The target type to be registered</typeparam>
        public void Register<T>() where T : new()
        {
            Register(typeof(T));
        }

        /// <summary>
        /// Registers a class to the DB-Side by type data. Base types are automatically registered aswell.
        /// </summary>
        /// <param name="type">The target type to be registered</param>
        public void Register(Type type)
        {
            MetadataClass metadataClass = metadataClassManager.GetOrRegister(type);

            // Check if base classes are registed
            if (metadataClass.HasBaseType)
                Register(metadataClass.BaseType.EnclosingType);
            
            if(!metadataClass.IsTableCreated)
            {
                targetStorage.Register(metadataClass);

                metadataClass.IsTableCreated = true;
            }
        }

        /// <summary>
        /// Represents an Upsert-Operation. The data is either updated or created depending on the primary key.
        /// </summary>
        /// <param name="data">The data that should be persisted</param>
        public void Persist(object data)
        {
            targetStorage.Upsert(data);
            dataTracker.Track(data);
        }

        /// <summary>
        /// Deletes the data on the DB-Side.
        /// </summary>
        /// <param name="data">The data that should be deleted.</param>
        public void Delete(object data)
        {
            if (!dataTracker.IsTracked(data))
                throw new InvalidOperationException("Cannot delete untracked data");

            targetStorage.Delete(data);
            dataTracker.Untrack(data);
        }

        /// <summary>
        /// Gets all objects of the given target type. This operation also supports polymorphic types. 
        /// </summary>
        /// <typeparam name="T">The type of which the data should be loaded</typeparam>
        /// <returns>A list of all objects that is saved on the DB-Side</returns>
        public IList<T> Get<T>() where T : new()
        {
            var elements = targetStorage.GetAll<T>();

            foreach (object obj in elements)
                dataTracker.Track(obj);

            return elements;
        }

        /// <summary>
        /// Gets all objects of the given target type by condition.
        /// </summary>
        /// <typeparam name="T">The type of which the data should be loaded</typeparam>
        /// <param name="whereExpression">A function expression that can be translated to SQL</param>
        /// <returns>A list of all objects that is saved on the DB-Side and matches the filter expression</returns>
        public IList<T> Get<T>(Expression<Func<T, bool>> whereExpression) where T : new()
        {
            ISQLQueryExtraData query = scriptGenerator.CreateQueryFromExpression(whereExpression);

            var elements = targetStorage.GetWhere(query, whereExpression);

            foreach (object obj in elements)
                dataTracker.Track(obj);

            return elements;
        }

        /// <summary>
        /// Starts a transaction. Once the action-function is finished the changes are automatically commited.
        /// If an exception happens then the changes are rolledback.
        /// </summary>
        /// <param name="transactionCode">The function that can perform DB-Actions that are commited or rolledback if successful.</param>
        public void WithTransaction(Action<TransactionProxy> transactionCode)
        {
            using var transaction = new TransactionProxy(targetStorage);
            try
            {
                transactionCode(transaction);
                transaction.Commit();
            } 
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Checks if a connection is active
        /// </summary>
        /// <returns>Connection is active</returns>
        public bool IsOpen() => engine.IsOpen();

        /// <summary>
        /// Opens an connection to the Database
        /// </summary>
        public void Open() => engine.Open();

        /// <summary>
        /// Disposes the object and closes the connection
        /// </summary>
        public void Dispose() => engine.Dispose();
    }
}

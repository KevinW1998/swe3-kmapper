﻿using KMapper.Core.Interfaces;
using KMapper.Core.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Tracker
{
    public class DataStateTrackerManager : IDataStateTracker
    {
        // TODO: Map by PK
        private Dictionary<Type, TrackInfo> cacheElements = new Dictionary<Type, TrackInfo>();
        private IMetadataClassProvider metadataClassProvider;

        public DataStateTrackerManager(IMetadataClassProvider metadataClassProvider)
        {
            this.metadataClassProvider = metadataClassProvider;
        }

        private TrackInfo GetTrackInfo(Type dataType)
        {
            if (!cacheElements.TryGetValue(dataType, out TrackInfo trackInfo))
            {
                trackInfo = new TrackInfo(metadataClassProvider.Get(dataType));
                cacheElements.Add(dataType, trackInfo);
            }
            return trackInfo;
        }

        public bool IsTracked(object data)
        {
            Type dataType = data.GetType();

            TrackInfo trackInfo = GetTrackInfo(dataType);
            object pk = trackInfo.Metadata.PrimaryKey.PropertyInfo.GetValue(data);

            return trackInfo.IsTracked(pk);
        }

        public void Track(object data)
        {
            Type dataType = data.GetType();
            
            TrackInfo trackInfo = GetTrackInfo(dataType);
            object pk = trackInfo.Metadata.PrimaryKey.PropertyInfo.GetValue(data);

            trackInfo.Track(pk);
        }

        public void Untrack(object data)
        {
            Type dataType = data.GetType();

            TrackInfo trackInfo = GetTrackInfo(dataType);
            object pk = trackInfo.Metadata.PrimaryKey.PropertyInfo.GetValue(data);

            trackInfo.Untrack(pk);
        }
    }
}

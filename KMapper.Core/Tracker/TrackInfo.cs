﻿using KMapper.Core.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core.Tracker
{
    public class TrackInfo
    {
        private HashSet<object> trackedPrimaryKey = new HashSet<object>();

        public MetadataClass Metadata { get; }

        public TrackInfo(MetadataClass metadata)
        {
            Metadata = metadata;
        }

        public void Track(object primaryKey) => trackedPrimaryKey.Add(primaryKey);

        public bool IsTracked(object primaryKey) => trackedPrimaryKey.Contains(primaryKey);

        public void Untrack(object primaryKey) => trackedPrimaryKey.Remove(primaryKey);
    }
}

﻿using KMapper.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Core
{
    /// <summary>
    /// Represents a running transaction.
    /// </summary>
    /// <seealso cref="PSQLMapper.WithTransaction(Action{TransactionProxy})"/>
    public class TransactionProxy : IDisposable
    {
        private ITransactionProvider provider;
        
        public bool Rollbacked { get; private set; }
        public bool Committed { get; private set; }


        internal TransactionProxy(ITransactionProvider provider)
        {
            this.provider = provider;
            provider.BeginTransaction();
        }

        /// <summary>
        /// Commits the transaction, all changes are persisted.
        /// </summary>
        public void Commit()
        {
            if (Committed)
                throw new InvalidOperationException("You cannot commit twice!");
            provider.Commit();
            Committed = true;
        }

        /// <summary>
        /// Rollbacks the changes
        /// </summary>
        public void Rollback()
        {
            if (Rollbacked)
                throw new InvalidOperationException("You cannot rollback twice!");
            provider.Rollback();
            Rollbacked = true;
        }

        public void Dispose()
        {
            // If an error happens, then we would rollback
            if(!Committed && !Rollbacked)
                provider.Rollback();
        }
    }
}

﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.MToNRelation
{
    public class Shop
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string Name { get; set; }

        [MToN("ShopCustomers")]
        public List<Customer> Customers { get; set; } = new List<Customer>();

    }
}

﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.MToNRelation
{
    public class Customer
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string Name { get; set; }

        [MToN("ShopCustomers")]
        public List<Shop> Shops { get; set; } = new List<Shop>();
    }
}

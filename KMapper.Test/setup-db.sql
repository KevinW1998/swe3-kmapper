-- Borrowed from https://stackoverflow.com/a/8099557/5082374 :)
DO
$do$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles  -- SELECT list can be empty for this
      WHERE  rolname = 'KMapperTestUser') THEN

      CREATE ROLE KMapperTestUser LOGIN PASSWORD 'KMapperTestPassword';
   END IF;
END
$do$;

CREATE DATABASE KMapperTestDB
   WITH OWNER KMapperTestUser 
   TEMPLATE template0
   ENCODING 'UTF8'
   TABLESPACE  pg_default
   CONNECTION LIMIT  -1;

-- Run in KMapperTestDB
ALTER SCHEMA public OWNER TO kmappertestuser;

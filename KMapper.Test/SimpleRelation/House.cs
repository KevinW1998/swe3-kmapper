﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.SimpleRelation
{
    public class House
    {
        [PrimaryKey]
        public int Id { get; set; }

        public Furniture MainTable { get; set; }
    }
}

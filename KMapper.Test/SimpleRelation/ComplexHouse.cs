﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.SimpleRelation
{
    public class ComplexHouse
    {
        [PrimaryKey]
        public int Id { get; set; }

        public List<Furniture> Tables { get; set; } = new List<Furniture>();
    }
}

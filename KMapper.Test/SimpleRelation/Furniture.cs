﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.SimpleRelation
{
    public class Furniture
    {
        [PrimaryKey]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.Inheritence
{
    public class RailVehicle : Vehicle
    {
        public int NumberOfWagons { get; set; }
    }
}

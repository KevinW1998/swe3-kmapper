﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.Inheritence
{
    public class Vehicle
    {
        [PrimaryKey]
        public int ID { get; set; }

        public float? MaxSpeed { get; set; }
    }
}

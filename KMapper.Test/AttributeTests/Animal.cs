﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.AttributeTests
{
    public class Animal
    {
        [PrimaryKey]
        [AutoIncrement]
        public int? Id { get; set; }

        public int LegCount { get; set; }
    }
}

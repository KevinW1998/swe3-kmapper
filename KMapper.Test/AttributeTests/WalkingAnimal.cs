﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.AttributeTests
{
    public class WalkingAnimal : Animal
    {
        public float WalkingSpeed { get; set; }
    }
}

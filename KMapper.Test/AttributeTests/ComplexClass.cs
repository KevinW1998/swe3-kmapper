﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.AttributeTests
{
    public class ComplexClass
    {
        [PrimaryKey]
        public int ID { get; set; }

        [Unique]
        public string UniqueColor { get; set; }
    }
}

using KMapper.Core;
using KMapper.Test.AttributeTests;
using KMapper.Test.Inheritence;
using KMapper.Test.MToNRelation;
using KMapper.Test.Simple;
using KMapper.Test.SimpleRelation;
using Npgsql;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace KMapper.Test
{
    public class MainTests
    {
        PSQLConfiguration config;

        [OneTimeSetUp]
        public void GlobalSetup()
        {
            config = PSQLConfiguration.FromJsonFile("sql-config.json");
            config.EnableDetailed = true;
        }

        [SetUp]
        public void Setup()
        {
            // clear the database
            using (var connection = new NpgsqlConnection($"Host={config.Host};Username={config.User};Password={config.Password};Database={config.Database}"))
            {
                connection.Open();

                using var cmd = new NpgsqlCommand("drop schema public cascade; create schema public;", connection);
                cmd.ExecuteNonQuery();
            }
        }

        // The order of the tests matches the implementation of the features.
        // The most recent tests are at the bottom.

        [Test]
        public void RegisterSimpleClass()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();

                mapper.Register<SimpleClass>();

                SimpleClass instance1 = new SimpleClass();
                instance1.MyID = 1;
                instance1.SimpleText = "Hello World";
                instance1.SimpleValue = 22;

                mapper.Persist(instance1);

                SimpleClass instance2 = new SimpleClass();
                instance2.MyID = 2;
                instance2.SimpleText = "Hello World 2";
                instance2.SimpleValue = 33;

                mapper.Persist(instance2);

                IList<SimpleClass> values = mapper.Get<SimpleClass>();

                Assert.AreEqual(2, values.Count);
                Assert.False(ReferenceEquals(instance1, values[0]), "Reference is equal, thus caching is probably active");
            }
        }

        [Test]
        public void SimpleCaching()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.EnableCaching();
                mapper.Register<SimpleClass>();

                // Force read cache
                mapper.Get<SimpleClass>();

                SimpleClass instance1 = new SimpleClass();
                instance1.MyID = 1;
                instance1.SimpleText = "Hello World";
                instance1.SimpleValue = 22;

                mapper.Persist(instance1);

                SimpleClass instance2 = new SimpleClass();
                instance2.MyID = 2;
                instance2.SimpleText = "Hello World 2";
                instance2.SimpleValue = 33;

                mapper.Persist(instance2);

                IList<SimpleClass> values = mapper.Get<SimpleClass>();

                Assert.AreEqual(2, values.Count);
                Assert.True(ReferenceEquals(instance1, values[0]), "References are not equal");
            }
        }

        [Test]
        public void OverwriteCaching()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<SimpleClass>();

                SimpleClass instance1 = new SimpleClass();
                instance1.MyID = 1;
                instance1.SimpleText = "Hello World";
                instance1.SimpleValue = 22;

                mapper.Persist(instance1);
            }

            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<SimpleClass>();
                mapper.EnableCaching();

                SimpleClass instance2 = new SimpleClass();
                instance2.MyID = 2;
                instance2.SimpleText = "Hello World 2";
                instance2.SimpleValue = 33;

                mapper.Persist(instance2);

                IList<SimpleClass> values = mapper.Get<SimpleClass>();

                Assert.AreEqual(2, values.Count);
                Assert.False(ReferenceEquals(instance2, values[1]), "References are equal, while they should not be");

                SimpleClass instance3 = new SimpleClass();
                instance3.MyID = 3;
                instance3.SimpleText = "Hello World 3";
                instance3.SimpleValue = 55;

                mapper.Persist(instance3);

                IList<SimpleClass> values2 = mapper.Get<SimpleClass>();
                Assert.AreEqual(3, values2.Count);
                Assert.True(ReferenceEquals(instance3, values2[2]), "References are not equal, while they should be");
            }

        }

        [Test]
        public void DerivedClass()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<Car>();

                Car c1 = new Car();
                c1.Color = "Blue";
                c1.MaxSpeed = null;
                c1.ID = 1;

                mapper.Persist(c1);

                Car c2 = new Car();
                c2.Color = "Grey";
                c2.MaxSpeed = 40.4f;
                c2.ID = 2;

                mapper.Persist(c2);

                IList<Car> cars = mapper.Get<Car>();
                Assert.AreEqual(2, cars.Count);

            }
        }

        [Test]
        public void DerivedClassDeep()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<Tram>();

                Tram t1 = new Tram();

                t1.MaxSpeed = null;
                t1.ID = 1;
                t1.NumberOfWagons = 5;
                t1.OptionalTramLine = null;

                mapper.Persist(t1);

                IList<Tram> cars = mapper.Get<Tram>();
                Assert.AreEqual(1, cars.Count);
            }
        }

        [Test]
        public void UpdateExistingElement()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<Car>();

                Car c1 = new Car();
                c1.Color = "Blue";
                c1.MaxSpeed = null;
                c1.ID = 1;

                mapper.Persist(c1);

                Car c2 = new Car();
                c2.Color = "Grey";
                c2.MaxSpeed = 40.4f;
                c2.ID = 2;

                mapper.Persist(c2);

                IList<Car> cars = mapper.Get<Car>();
                Assert.AreEqual(2, cars.Count);

                c2.MaxSpeed = 50.5f;
                mapper.Persist(c2);
            }
        }

        [Test]
        public void DerivedClassPolymorphLoad()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<Tram>();
                mapper.Register<Car>();


                Tram t1 = new Tram();

                t1.MaxSpeed = null;
                t1.ID = 1;
                t1.NumberOfWagons = 5;
                t1.OptionalTramLine = null;

                mapper.Persist(t1);

                RailVehicle genericRailVehicle = new RailVehicle();

                genericRailVehicle.ID = 2;
                genericRailVehicle.MaxSpeed = 20.0f;
                genericRailVehicle.NumberOfWagons = 10;

                mapper.Persist(genericRailVehicle);

                Car car = new Car();

                car.ID = 3;
                car.MaxSpeed = 100.0f;
                car.Color = "Green";

                mapper.Persist(car);

                IList<RailVehicle> railVehicles = mapper.Get<RailVehicle>();
                Assert.AreEqual(2, railVehicles.Count);

                Assert.IsTrue(railVehicles[0] is Tram);
                Assert.IsTrue(railVehicles[1] is RailVehicle);

                IList<Vehicle> vehicles = mapper.Get<Vehicle>();
                Assert.AreEqual(3, vehicles.Count);

                Assert.IsTrue(vehicles[0] is Tram);
                Assert.IsTrue(vehicles[1] is RailVehicle);
                Assert.IsTrue(vehicles[2] is Car);

            }
        }

        [Test]
        public void DeleteElement()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<Tram>();

                Tram t1 = new Tram();

                t1.MaxSpeed = null;
                t1.ID = 1;
                t1.NumberOfWagons = 5;
                t1.OptionalTramLine = null;

                mapper.Persist(t1);

                IList<RailVehicle> railVehicles = mapper.Get<RailVehicle>();
                Assert.AreEqual(1, railVehicles.Count);

                mapper.Delete(t1);

                railVehicles = mapper.Get<RailVehicle>();

                Assert.AreEqual(0, railVehicles.Count);

            }
        }

        [Test]
        public void DerivedClassPolymorphLoadWithCache()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<Tram>();
                mapper.Register<Car>();
                mapper.EnableCaching();

                Tram t1 = new Tram();

                t1.MaxSpeed = null;
                t1.ID = 1;
                t1.NumberOfWagons = 5;
                t1.OptionalTramLine = null;

                mapper.Persist(t1);

                RailVehicle genericRailVehicle = new RailVehicle();

                genericRailVehicle.ID = 2;
                genericRailVehicle.MaxSpeed = 20.0f;
                genericRailVehicle.NumberOfWagons = 10;

                mapper.Persist(genericRailVehicle);

                Car car = new Car();

                car.ID = 3;
                car.MaxSpeed = 100.0f;
                car.Color = "Green";

                mapper.Persist(car);

                IList<RailVehicle> railVehicles = mapper.Get<RailVehicle>();
                Assert.AreEqual(2, railVehicles.Count);

                Assert.IsTrue(railVehicles[0] is Tram);
                Assert.IsTrue(railVehicles[1] is RailVehicle);

                IList<Vehicle> vehicles = mapper.Get<Vehicle>();
                Assert.AreEqual(3, vehicles.Count);

                Assert.IsTrue(vehicles[0] is Tram);
                Assert.IsTrue(vehicles[1] is RailVehicle);
                Assert.IsTrue(vehicles[2] is Car);

                IList<Car> carVehicles = mapper.Get<Car>();
                Assert.AreEqual(1, carVehicles.Count);

                mapper.Delete(vehicles[1]);

                IList<Vehicle> vehiclesAfterDelete = mapper.Get<Vehicle>();
                Assert.IsTrue(vehiclesAfterDelete[1] is Car);
                Assert.IsTrue(ReferenceEquals(vehiclesAfterDelete[1], vehicles[2]));
            }
        }

        [Test]
        public void TestQuery()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<Car>();

                for (int i = 0; i < 20; i++)
                {
                    Car car = new Car();
                    car.Color = "Red";
                    car.MaxSpeed = 10.0f + (i * 10.0f);
                    car.ID = i;

                    mapper.Persist(car);
                }

                for (int i = 0; i < 20; i++)
                {
                    Car car = new Car();
                    car.Color = "Yellow";
                    car.MaxSpeed = 10.0f + (i * 10.0f);
                    car.ID = i + 20;

                    mapper.Persist(car);
                }

                for (int i = 0; i < 20; i++)
                {
                    Car car = new Car();
                    car.Color = "Green";
                    car.MaxSpeed = 10.0f + (i * 10.0f);
                    car.ID = i + 40;

                    mapper.Persist(car);
                }

                for (int i = 0; i < 10; i++)
                {
                    Car car = new Car();
                    car.Color = "Blue";
                    car.MaxSpeed = null;
                    car.ID = i + 60;

                    mapper.Persist(car);
                }

                IList<Car> slowRedCars = mapper.Get<Car>(c => c.MaxSpeed < 65.0f && c.Color == "Red");
                Assert.AreEqual(6, slowRedCars.Count);

                IList<Car> yellowCarsAndSlowRedCars = mapper.Get<Car>(c => c.MaxSpeed < 65.0f && c.Color == "Red" || c.Color == "Yellow");
                Assert.AreEqual(26, yellowCarsAndSlowRedCars.Count);

                IList<Car> slowYellowAndRedCars = mapper.Get<Car>(c => c.MaxSpeed < 65.0f && (c.Color == "Red" || c.Color == "Yellow"));
                Assert.AreEqual(12, slowYellowAndRedCars.Count);

                IList<Car> carsWithoutMaxSpeed = mapper.Get<Car>(c => c.MaxSpeed == null);
                Assert.AreEqual(10, carsWithoutMaxSpeed.Count);
            }
        }

        [Test]
        public void TestQueryWithCache()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<Car>();
                mapper.EnableCaching();

                Car car1 = new Car();
                car1.Color = "Red";
                car1.MaxSpeed = 10.0f;
                car1.ID = 1;

                mapper.Persist(car1);

                Car car2 = new Car();
                car2.Color = "Red";
                car2.MaxSpeed = 20.0f;
                car2.ID = 2;

                mapper.Persist(car2);

                Car car3 = new Car();
                car3.Color = "Red";
                car3.MaxSpeed = 30.0f;
                car3.ID = 3;

                mapper.Persist(car3);

                // Get cache filled
                IList<Car> cars = mapper.Get<Car>();
                Assert.AreEqual(3, cars.Count);
                Assert.AreEqual(1, cars[0].ID);

                IList<Car> filteredCars = mapper.Get<Car>(c => c.MaxSpeed < 25.0f);
                Assert.AreEqual(2, filteredCars.Count);
                Assert.IsTrue(ReferenceEquals(cars[0], filteredCars[0]));
            }
        }

        [Test]
        public void TestAttributes()
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();
                mapper.Register<ComplexClass>();

                ComplexClass cls1 = new ComplexClass();
                cls1.ID = 1;
                cls1.UniqueColor = "Red";

                mapper.Persist(cls1);

                ComplexClass cls2 = new ComplexClass();
                cls2.ID = 2;
                cls2.UniqueColor = "Yellow";

                mapper.Persist(cls2);
            }
        }

        public void RegisterWithTransaction(bool withCache)
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();

                mapper.Register<SimpleClass>();
                if(withCache)
                    mapper.EnableCaching();

                SimpleClass instance1 = new SimpleClass();
                instance1.MyID = 1;
                instance1.SimpleText = "Hello World";
                instance1.SimpleValue = 22;

                SimpleClass instance2 = new SimpleClass();
                instance2.MyID = 2;
                instance2.SimpleText = "Hello World 2";
                instance2.SimpleValue = 33;


                mapper.WithTransaction(t =>
                {
                    mapper.Persist(instance1);
                    mapper.Persist(instance2);
                });

                IList<SimpleClass> values = mapper.Get<SimpleClass>();

                Assert.AreEqual(2, values.Count);
                Assert.False(ReferenceEquals(instance1, values[0]), "Reference is equal, thus caching is probably active");

                SimpleClass instance3 = new SimpleClass();
                instance3.MyID = 3;
                instance3.SimpleText = "Hello World 3";
                instance3.SimpleValue = 44;

                SimpleClass instance4 = new SimpleClass();
                instance4.MyID = 4;
                instance4.SimpleText = "Hello World 4";
                instance4.SimpleValue = 55;

                try
                {
                    mapper.WithTransaction(t =>
                    {
                        mapper.Persist(instance3);
                        mapper.Persist(instance4);

                        throw new TestException();
                    });
                }
                catch (TestException _)
                { }


                IList<SimpleClass> newValues = mapper.Get<SimpleClass>();
                Assert.AreEqual(2, newValues.Count);
            }
        }

        [Test]
        public void RegisterWithTransactionAndCache()
        {
            RegisterWithTransaction(true);
        }

        [Test]
        public void RegisterWithTransactionAndWithoutCache()
        {
            RegisterWithTransaction(false);
        }

        public void TestAutoIncrement(bool withCache)
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();

                mapper.Register<WalkingAnimal>();
                if (withCache)
                    mapper.EnableCaching();

                WalkingAnimal instance1 = new WalkingAnimal();
                instance1.LegCount = 10;
                instance1.WalkingSpeed = 10.4f;
                
                mapper.Persist(instance1);

                Animal instance2 = new Animal();
                instance2.LegCount = 2;

                mapper.Persist(instance2);

                WalkingAnimal instance3 = new WalkingAnimal();
                instance3.LegCount = 120;
                instance3.WalkingSpeed = 20.4f;

                mapper.Persist(instance3);

                Assert.IsNotNull(instance1.Id);
                Assert.IsNotNull(instance2.Id);
                Assert.IsNotNull(instance3.Id);

                IList<WalkingAnimal> walkingAnimals = mapper.Get<WalkingAnimal>();
                Assert.AreEqual(2, walkingAnimals.Count);
            }
        }

        [Test]
        public void TestAutoIncrementWithCache()
        {
            TestAutoIncrement(true);
        }

        [Test]
        public void TestAutoIncrementWithoutCache()
        {
            TestAutoIncrement(false);
        }

        public void TestSimpleRelation(bool withCache)
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();

                mapper.Register<Furniture>();
                mapper.Register<House>();
                if (withCache)
                    mapper.EnableCaching();

                Furniture mf = new Furniture();
                mf.Id = 1;
                mf.Name = "Cool Table";

                mapper.Persist(mf);

                House house = new House();
                house.Id = 1;
                house.MainTable = mf;

                mapper.Persist(house);

                IList<House> houses = mapper.Get<House>();
                Assert.AreEqual(1, houses.Count);
                Assert.NotNull(houses[0].MainTable);
            }
        }

        [Test]
        public void TestSimpleRelationWithCache()
        {
            TestSimpleRelation(true);
        }

        [Test]
        public void TestSimpleRelationWithoutCache()
        {
            TestSimpleRelation(false);
        }

        public void Test1ToNRelation(bool withCache)
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();

                mapper.Register<Furniture>();
                mapper.Register<ComplexHouse>();
                if (withCache)
                    mapper.EnableCaching();

                ComplexHouse house = new ComplexHouse();
                
                for (int i = 0; i < 10; i++)
                {
                    Furniture mf = new Furniture();
                    mf.Id = i;
                    mf.Name = "Cool Table " + i;

                    mapper.Persist(mf);

                    house.Tables.Add(mf);
                }


                house.Id = 1;

                mapper.Persist(house);

                IList<ComplexHouse> houses = mapper.Get<ComplexHouse>();
                Assert.AreEqual(1, houses.Count);
                Assert.AreEqual(10, houses[0].Tables.Count);
            }
        }

        [Test]
        public void Test1ToNRelationWithCache()
        {
            Test1ToNRelation(true);
        }

        [Test]
        public void Test1ToNRelationWithoutCache()
        {
            Test1ToNRelation(false);
        }

        public void TestMToNRelation(bool withCache)
        {
            using (PSQLMapper mapper = new PSQLMapper(config))
            {
                mapper.Open();

                mapper.Register<Customer>();
                mapper.Register<Shop>();
                if (withCache)
                    mapper.EnableCaching();

                Shop sh1 = new Shop();
                sh1.Id = 1;
                sh1.Name = "Coffee House";

                mapper.Persist(sh1);

                Shop sh2 = new Shop();
                sh2.Id = 2;
                sh2.Name = "Car Dealer";

                mapper.Persist(sh2);

                Shop sh3 = new Shop();
                sh3.Id = 3;
                sh3.Name = "Grocery Store";

                mapper.Persist(sh3);

                Customer c1 = new Customer();
                c1.Id = 1;
                c1.Name = "Lukas";
                c1.Shops.Add(sh1);
                c1.Shops.Add(sh3);

                mapper.Persist(c1);

                Customer c2 = new Customer();
                c2.Id = 2;
                c2.Name = "Max";
                c2.Shops.Add(sh2);
                c2.Shops.Add(sh3);

                mapper.Persist(c2);

                var shops = mapper.Get<Shop>();

                Assert.AreEqual(2, shops[2].Customers.Count);
                Assert.IsTrue(ReferenceEquals(shops[0].Customers[0].Shops[0], shops[0]));
            }
        }

        [Test]
        public void TestMToNRelationWithCache()
        {
            TestMToNRelation(true);
        }

        [Test]
        public void TestMToNRelationWithoutCache()
        {
            TestMToNRelation(false);
        }
    }
}
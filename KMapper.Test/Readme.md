﻿# KMapper.Test

This document describes how you can setup your testing environment.

## Method One: Using Docker

In the main folder there is a file called `docker-compose.yml`. Navigate to this folder and run `docker-compose up -d`. Now your testing environment should be setup.

## Method Two: Manual

Before running any tests, be sure to load in `setup-db.sql` in your postgresql database. 
This script must be run with a user, that has the rights to create databases (or alternative has super-user rights).

If you're working on a Windows machine with a default postgres installation, you can run `setup-db.bat`.



﻿using KMapper.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KMapper.Test.Simple
{
    public class SimpleClass
    {
        [PrimaryKey]
        public int MyID { get; set; }

        public int SimpleValue { get; set; }

        public string SimpleText { get; set; }
    }
}
